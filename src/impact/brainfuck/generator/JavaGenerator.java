package impact.brainfuck.generator;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import impact.brainfuck.operation.Operation;

/**
 * This class can generate functional Java code from the instruction list
 * provided by the reader.
 * 
 * @author Elliot Kauffmann
 * @version 1.0
 */
public class JavaGenerator {

	/** Map that links instruction enumeration instances to Java code strings */
	private Map<Operation, String> instructionsToJava;

	/**
	 * This method takes an empty StringBuilder and appends it with the
	 * beginning of a Java program capable of executing BrainF*ck instructions.
	 * 
	 * @param out - the StringBuilder to be initialized with Java code
	 */
	private void initializeStringBuilder(StringBuilder out) {
		out.append("import java.io.IOException;").append('\n').append('\n').append("/*").append('\n')
				.append(" * Note: this file should be called Main.java or this class should be renamed to the filename to properly compile")
				.append('\n').append(" */").append('\n').append("public class Main {").append('\n').append('\n')
				.append("    public static void main(String[] args) throws IOException {").append('\n').append('\n').append("        int p = 0;")
				.append('\n').append("        byte[] memory = new byte[30000];").append('\n').append('\n')
				.append("        // Code generation from BrainF*ck to Java begins here").append('\n');
	}

	/**
	 * This method takes a StringBuilder containing a Java program capable of
	 * executing BrainF*ck instructions that needs to be ended and adds the end
	 * to it.
	 * 
	 * @param out - the StringBuilder containing a Java program to be ended.
	 */
	private void endStringBuilder(StringBuilder out) {
		out.append("        // Code generation ends here").append('\n').append('\n').append("    }").append('\n').append("}");

	}

	/**
	 * This constructor builds a new JavaGenerator object and inserts the
	 * translations of BrainF*ck instructions in Java code in its map.
	 */
	public JavaGenerator() {
		instructionsToJava = new HashMap<>();
		instructionsToJava.put(Operation.INCR, "memory[p]++;");
		instructionsToJava.put(Operation.DECR, "memory[p]--;");
		instructionsToJava.put(Operation.LEFT, "p--;");
		instructionsToJava.put(Operation.RIGHT, "p++;");
		instructionsToJava.put(Operation.IN, "System.in.read(memory, p, 1);");
		instructionsToJava.put(Operation.OUT, "System.out.write(memory[p]);");
		instructionsToJava.put(Operation.JUMP, "while (memory[p] != 0) {");
		instructionsToJava.put(Operation.BACK, "}");
	}

	/**
	 * This method takes an initialized StringBuilder and appends it with the
	 * translations in Java code of the instructions contained in the List.
	 * 
	 * @param instructions - the List of BrainF*ck instructions to be translated
	 * @param output - the Output file or stream
	 */
	public void generate(List<Operation> instructions, PrintStream output) {

		StringBuilder out = new StringBuilder();
		initializeStringBuilder(out);
		int indent = 2;

		for (Operation currentOperation : instructions) {
			if (currentOperation == Operation.BACK)
				indent--;
			for (int i = 0; i < indent; i++)
				out.append("    ");
			out.append(instructionsToJava.get(currentOperation)).append('\n');
			if (currentOperation == Operation.JUMP)
				indent++;
		}

		endStringBuilder(out);
		output.println(out.toString());
	}

}
