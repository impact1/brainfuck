package impact.brainfuck.exception;

/**
 * Exception thrown when an error happened when handling the input file supplied
 * with -i.
 * 
 * @author Guillaume Andre
 * @version 2.0
 */
public class InputException extends Exception {

	private static final long serialVersionUID = 3276595937451734685L;

	public InputException() {
		super("Could not parse input file.");
		System.out.println(this.getMessage());
		System.exit(9);
	}
}
