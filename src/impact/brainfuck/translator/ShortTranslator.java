package impact.brainfuck.translator;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import impact.brainfuck.exception.SyntaxErrorException;
import impact.brainfuck.operation.Operation;

/**
 * @author Florian Bourniquel
 * @version 1.0
 *
 * the translator instruction to short
 */
public class ShortTranslator implements Translator {

	private Map<Operation, String> shorts = new HashMap<>();

	public ShortTranslator() {
		shorts.put(Operation.INCR, "+");
		shorts.put(Operation.DECR, "-");
		shorts.put(Operation.LEFT, "<");
		shorts.put(Operation.RIGHT, ">");
		shorts.put(Operation.OUT, ".");
		shorts.put(Operation.IN, ",");
		shorts.put(Operation.JUMP, "[");
		shorts.put(Operation.BACK, "]");
	}

	/**
	 * Translation of the List
	 * 
	 * @param params given by reader to translate
	 */
	@Override
	public void translate(List<Operation> params, PrintStream output) throws SyntaxErrorException {
		String result = "";
		for (int i = 0; i < params.size(); i++) {
			result += shorts.get(params.get(i));
		}
		output.println(result);
	}
}
