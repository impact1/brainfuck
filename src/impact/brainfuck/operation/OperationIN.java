package impact.brainfuck.operation;

import impact.brainfuck.exception.AsciiException;
import impact.brainfuck.exception.InputException;
import impact.brainfuck.memory.Memory;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * @author Guillaume Andre
 * @version 2.0
 *          Implementation of CommandOperation.
 *          Inputs a char as its value in the current memory cell.
 */
public class OperationIN implements CommandOperation {

    private static BufferedReader br = null;
    private static Scanner sc = null;
    private int inputCount = 0;

    public static void setBr(BufferedReader br) {
        OperationIN.br = br;
    }

    public static void setSc(Scanner sc) {
        OperationIN.sc = sc;
    }

    public static BufferedReader getBr() {
        return br;
    }

    /**
     * Inputs a char as its value in the current memory cell.
     *
     * @param memory                  - the memory in its current state
     * @param currentInstructionIndex - the instruction pointer
     * @return the instruction pointer moved to the next instruction
     */
    @Override
    public int doOperation(Memory memory, int currentInstructionIndex) throws InputException, AsciiException {
        if (br == null) {
            String answer;
            while (true) {
                System.out.print("Input " + (inputCount + 1) + ":\n");
                answer = sc.nextLine();
                if (answer.length() != 1) {
                    System.out.print("Input must be one character.\n");
                } else if ((int) answer.charAt(0) > 255) {
                    System.out.println("Input must be an ASCII character.");
                } else {
                    break;
                }
            }
            inputCount++;
            memory.getMemory()[memory.getCurrentCellIndex()] = (byte) (answer.charAt(0) - 128);
        } else {
            try {
                int num = br.read();
                if (num == -1) {
                    return currentInstructionIndex + 1;
                }
                if (num < 0 || num > 255) {
                    throw new AsciiException();
                }
                memory.getMemory()[memory.getCurrentCellIndex()] = (byte) (num - 128);
            } catch (IOException e) {
                throw new InputException();
            }
        }
        return currentInstructionIndex + 1;
    }
}
