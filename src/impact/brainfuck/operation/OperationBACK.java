package impact.brainfuck.operation;

import impact.brainfuck.exception.LoopException;
import impact.brainfuck.memory.Memory;

import java.util.Map;

/**
 * @author Guillaume Andre, Elliot Kauffmann
 * @version 2.1
 *          Implementation of CommandOperation.
 *          Goes back to the associated JUMP if the current cell value is not 0.
 */
public class OperationBACK implements CommandOperation {

    private static Map<Integer, Integer> jumpTable;

    public static void setJumpTable(Map<Integer, Integer> jumpTable) { OperationBACK.jumpTable = jumpTable; }

    /**
     * Does nothing if current cell is at 0.
     * Returns the index of the corresponding JUMP otherwise.
     *
     * @param memory                  - the memory in its current state
     * @param currentInstructionIndex - the instruction pointer
     * @return where the instruction pointer should move
     * @throws LoopException - if no corresponding JUMP was found
     */
    public int doOperation(Memory memory, int currentInstructionIndex) throws LoopException {
        if (memory.getMemory()[memory.getCurrentCellIndex()] == -128) {
            return currentInstructionIndex + 1;
        } else {
            return jumpTable.get(currentInstructionIndex) + 1;
        }
    }
}
