package impact.brainfuck.main;

import static junit.framework.TestCase.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import impact.brainfuck.macro.Function;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.rules.TemporaryFolder;

import impact.brainfuck.exception.BadArgumentsException;
import impact.brainfuck.exception.NotFoundException;

/**
 * Test class for argument manager.
 *
 * @author Guillaume Andre
 */
public class ArgumentManagerTest {

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    @After
    public void tearDown() {
        Function.allowFunctions = true;
    }

    @Test
    public void correctAll() throws NotFoundException, BadArgumentsException, IOException {
        File program = folder.newFile("testProgram");
        File input = folder.newFile("testInput");
        File output = folder.newFile("testOutput");
        ArgumentManager am = new ArgumentManager(new String[]{"-p", program.getAbsolutePath(), "-i", input.getAbsolutePath(), "-o", output.getAbsolutePath(), "-m",
                "--check", "--translate", "--rewrite", "--trace", "./bfck"});
        am.checkArguments();
        assertEquals(program, am.getProgram());
        assertEquals(input, am.getInput());
        assertEquals("testProgram.log", am.getTracelog().getName());
        Set<String> options = am.getOptions();
        assert options.contains("--check");
        assert options.contains("--translate");
        assert options.contains("--rewrite");
    }

    @Test
    public void traceBeforeProgram() throws NotFoundException, BadArgumentsException {
        ArgumentManager am = new ArgumentManager(new String[]{"--trace"});
        exit.expectSystemExitWithStatus(6);
        am.checkArguments();
    }

    @Test
    public void notFound() throws NotFoundException, BadArgumentsException {
        ArgumentManager am = new ArgumentManager(new String[]{"-p", "nope.bf"});
        exit.expectSystemExitWithStatus(3);
        am.checkArguments();
    }

    @Test
    public void invalidOptions() throws NotFoundException, BadArgumentsException {
        ArgumentManager am = new ArgumentManager(new String[]{"--check", "--nope"});
        exit.expectSystemExitWithStatus(6);
        am.checkArguments();
    }

    @Test
    public void multipleOutput() throws IOException, NotFoundException, BadArgumentsException {
        File program = folder.newFile("testProgram");
        File output = folder.newFile("testOutput");
        File output2 = folder.newFile("testOutput2");
        ArgumentManager am = new ArgumentManager(new String[]{"-p", program.getAbsolutePath(), "-o", output.getAbsolutePath(), "-o", output2.getAbsolutePath()});
        exit.expectSystemExitWithStatus(6);
        am.checkArguments();
    }

    @Test
    public void multipleInput() throws IOException, NotFoundException, BadArgumentsException {
        File program = folder.newFile("testProgram");
    	File input = folder.newFile("testInput");
		File input2 = folder.newFile("testInput2");
		ArgumentManager am = new ArgumentManager(new String[] {"-p", program.getAbsolutePath(), "-i", input.getAbsolutePath(), "-i", input2.getAbsolutePath() });
        exit.expectSystemExitWithStatus(6);
        am.checkArguments();
    }

    @Test
    public void multipleProgram() throws IOException, NotFoundException, BadArgumentsException {
        File program = folder.newFile("testProgram");
        File program2 = folder.newFile("testProgram2");
        ArgumentManager am = new ArgumentManager(new String[]{"-p", program.getAbsolutePath(), "-p", program2.getAbsolutePath()});
        exit.expectSystemExitWithStatus(6);
        am.checkArguments();
    }

    @Test
    public void minimumArguments() throws IOException, NotFoundException, BadArgumentsException {
        File program = folder.newFile();
        ArgumentManager am = new ArgumentManager(new String[]{"-p", program.getAbsolutePath()});
        am.checkArguments();
        assertEquals(program, am.getProgram());
        assertEquals(null, am.getInput());
        assert am.getOptions().isEmpty();
    }

    @Test
    public void withMetrics() throws IOException, NotFoundException, BadArgumentsException {
        File program = folder.newFile();
        ArgumentManager am = new ArgumentManager(new String[]{"-p", program.getAbsolutePath(), "-m"});
        am.checkArguments();
        assert Metrics.metricsOn;
    }

    @Test
    public void noArguments() throws NotFoundException, BadArgumentsException {
        ArgumentManager am = new ArgumentManager(new String[]{});
        exit.expectSystemExitWithStatus(6);
        am.checkArguments();
    }

    @Test
    public void noProgram() throws IOException, NotFoundException, BadArgumentsException {
        File output = folder.newFile("testOutput");
        ArgumentManager am = new ArgumentManager(new String[]{"-o", output.getAbsolutePath(), "--check"});
        exit.expectSystemExitWithStatus(6);
        am.checkArguments();
    }

    @Test
    public void iAndNoFile() throws IOException, NotFoundException, BadArgumentsException {
        File program = folder.newFile();
        ArgumentManager am = new ArgumentManager(new String[]{"-p", program.getAbsolutePath(), "-i"});
        exit.expectSystemExitWithStatus(6);
        am.checkArguments();
    }

    @Test
    public void pAndNoFile() throws IOException, NotFoundException, BadArgumentsException {
        ArgumentManager am = new ArgumentManager(new String[]{"-p"});
        exit.expectSystemExitWithStatus(6);
        am.checkArguments();
    }

    @Test
    public void oAndNoFile() throws IOException, NotFoundException, BadArgumentsException {
        File program = folder.newFile();
        ArgumentManager am = new ArgumentManager(new String[]{"-p", program.getAbsolutePath(), "-o"});
        exit.expectSystemExitWithStatus(6);
        am.checkArguments();
    }
}
