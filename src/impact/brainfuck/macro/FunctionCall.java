package impact.brainfuck.macro;

import impact.brainfuck.exception.SyntaxErrorException;
import impact.brainfuck.operation.Operation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Guillaume André
 *         <p>
 *         Used to store the functions that should be called at execution time, with their parameters.
 */
public class FunctionCall {

    private List<String> allInputs;
    private String input;
    private Function function;
    private List<Integer> params;
    private boolean hasOutput;
    private Map<Integer, List<FunctionCall>> nestedFunctions;

    /**
     * Creates a new FunctionCall.
     *
     * @param allInputs - the inputs for this function
     * @param function  - the function used
     * @param params    - the parameters for this function
     */
    public FunctionCall(List<String> allInputs, Function function, List<Integer> params) {
        this.allInputs = allInputs;
        this.function = function;
        this.params = params;
        this.nestedFunctions = new HashMap<>();
        if (function.isDefaultInput()) {
            this.input = this.allInputs.remove(0);
        } else {
            this.input = null;
        }
    }

    public Function getFunction() {
        return function;
    }

    /**
     * Returns a map of the functions calls for the functions contained within the "main" function.
     * The keys of the map corresponds to the position within the instructions obtained with the loadList() method.
     */
    public Map<Integer, List<FunctionCall>> getNestedFunctions() {
        return nestedFunctions;
    }

    /**
     * Returns the custom input to be used for this function instructions (not for the function calls).
     * Returns null if this function does not use a custom input.
     */
    public String getInput() {
        return input;
    }

    /**
     * Returns the list of all the inputs used by the sub-functions in this function.
     */
    public List<String> getAllInputs() {
        return allInputs;
    }

    /**
     * Checks whether this function has a return value or not.
     *
     * @return true if the function must return something, false otherwise
     */
    public boolean hasOutput() {
        return function.isAllowReturn();
    }

    /**
     * Constructs this functions instructions list, and also the function calls of its sub-functions.
     * The sub-functions calls are placed in the nestedFunctions Map.
     *
     * @return this function instructions list
     * @throws SyntaxErrorException - if an incorrect number of parameters or inputs was used
     */
    public List<Operation> loadList() throws SyntaxErrorException {
        return function.toList(params, allInputs, 0, nestedFunctions, true);
    }
}
