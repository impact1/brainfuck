package impact.brainfuck.translator;

import java.io.PrintStream;
import java.util.List;

import impact.brainfuck.exception.OutputException;
import impact.brainfuck.exception.SyntaxErrorException;
import impact.brainfuck.operation.Operation;

/**
 * @author      Florian Bourniquel
 * @version     1.0
 *
 * Interface of the translator
 */
public interface Translator {
	
    void translate (List<Operation> params, PrintStream output) throws SyntaxErrorException, OutputException;
}
