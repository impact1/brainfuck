package impact.brainfuck.exception;

/**
 * Thrown when the -p and -i statements are unable to find their associated
 * file.
 * 
 * @author Guillaume Andre
 * @version 1.0
 */
public class NotFoundException extends Exception {

	private static final long serialVersionUID = 9177685846324485098L;

	/**
	 * @param file - the file that was not found.
	 */
	public NotFoundException(String file) {
		super("File " + file + " was not found.");
		System.out.println(this.getMessage());
		System.exit(3);
	}

}
