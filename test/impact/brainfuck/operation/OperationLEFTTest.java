package impact.brainfuck.operation;

import impact.brainfuck.memory.Memory;
import impact.brainfuck.operation.OperationLEFT;
import impact.brainfuck.operation.OperationRIGHT;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import static org.junit.Assert.assertEquals;

/**
 * @author Florian Bourniquel
 * @version 1.0
 * Test for OperationLEFT
 *
 *          if this throws NullPointerException on all 3 tests, that's because you're using IntelliJ.
 *          Seriously though, it comes from a bug with System.out and System.in, and is an IDE bug.
 *          Don't worry too much about it.
 *          Running this test class alone should give correct results.
 */
public class OperationLEFTTest {

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void doOperation() throws Exception {
        Memory memory = new Memory();
        OperationLEFT left = new OperationLEFT();
        OperationRIGHT right = new OperationRIGHT();
        right.doOperation(memory,0);
        right.doOperation(memory,0);
        right.doOperation(memory,0);
        left.doOperation(memory,0);
        left.doOperation(memory,0);
        assertEquals(1,memory.getCurrentCellIndex());
    }

    @Test //(expected = PointerException.class)
    public void PointerException () throws Exception {
        Memory memory = new Memory();
        OperationLEFT left = new OperationLEFT();
        exit.expectSystemExitWithStatus(2);
        left.doOperation(memory,0);
        //NullPointerException ? see above.
    }

}