package impact.brainfuck.interpreter;

import impact.brainfuck.check.LoopChecker;
import impact.brainfuck.exception.*;
import impact.brainfuck.macro.FunctionCall;
import impact.brainfuck.main.Metrics;
import impact.brainfuck.memory.Memory;
import impact.brainfuck.operation.Operation;
import impact.brainfuck.reader.TextReader;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * @author Jeremy Lara, Elliot Kauffmann
 * @version 1.1
 */
public class InterpreterTest {

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();
    private StringBuilder displayMetrics;
    private List<Operation> instructions = new ArrayList<>();

    @Before
    public void setUp() {
        displayMetrics = new StringBuilder();
        displayMetrics.append("\nThis program contains 18")
                .append(" instruction(s) and was executed in 0 ms.\n")
                .append("The execution pointer was moved 28")
                .append(" time(s), and the data pointer 11 time(s).\n")
                .append("The memory was changed 13")
                .append(" time(s) and was read 4 time(s).\n");
    }

    private Map<Integer, List<FunctionCall>> createFunctionList(String content)
            throws NotFoundException, ReadException, SyntaxErrorException, IOException, BadArgumentsException {
        File file = folder.newFile();
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(content);
        bw.close();
        TextReader reader = new TextReader(file);
        instructions = reader.read();
        return reader.getFunctions();
    }

    @Test
    public void measureMetrics() throws InputException, OutputException, PointerException, SyntaxErrorException,
            CellOverflowException, AsciiException, LoopException, OutOfMemory {
        Memory memory = new Memory();
        List<Operation> instructions = new ArrayList<>();
        //+>+<->>+++[-<+>]>+
        instructions.add(Operation.INCR);
        instructions.add(Operation.RIGHT);
        instructions.add(Operation.INCR);
        instructions.add(Operation.LEFT);
        instructions.add(Operation.DECR);
        instructions.add(Operation.RIGHT);
        instructions.add(Operation.RIGHT);
        instructions.add(Operation.INCR);
        instructions.add(Operation.INCR);
        instructions.add(Operation.INCR);
        instructions.add(Operation.JUMP);
        instructions.add(Operation.DECR);
        instructions.add(Operation.LEFT);
        instructions.add(Operation.INCR);
        instructions.add(Operation.RIGHT);
        instructions.add(Operation.BACK);
        instructions.add(Operation.RIGHT);
        instructions.add(Operation.INCR);
        Metrics metrics = new Metrics();
        Metrics.metricsOn = true;
        Interpreter interpreter = new Interpreter(memory, instructions, null, null, null, (new LoopChecker().check(instructions)),new HashMap<>(), metrics);
        interpreter.run();

        //interpreter.displayMetrics();
        //---------------------------------Details-----------------------------------------
        //PROG_SIZE = 18 = instructions.size()
        //EXEC_TIME cannot be tested as it changes every time
        //EXEC_MOVE = 28 = (1*INCR + 1*RIGHT + 1*INCR + 1*LEFT + 1*DECR  + 2*RIGHT + 3*INCR)
        //               + (1*JUMP + 3*DECR  + 3*LEFT + 3*INCR + 3*RIGHT + 3*BACK)
        //               + (1*RIGHT + 1*INCR)
        //               = 10 + 16 + 2
        //DATA_MOVE = 11 = (Total LEFT) + (Total RIGHT) = (4*LEFT) + (7*RIGHT)
        //DATA_WRITE = 13 =  Total(IN + INCR + DECR) = (0*IN) + (9*INCR) + (4*DECR)
        //DATA_READ = 4 = Total(JUMP + BACK + OUT) = (0*OUT) + (1*JUMP) + (3*BACK)
        //---------------------------------------------------------------------------------
        assertEquals(String.valueOf(displayMetrics), metrics.displayMetrics());
    }

    //****************************
    //Procedures & Functions tests
    //****************************

    @Test
    public void simpleProcedure()
            throws InputException, OutputException, SyntaxErrorException,
            ReadException, NotFoundException, IOException, LoopException,
            AsciiException, CellOverflowException, PointerException, OutOfMemory, BadArgumentsException {

        Memory got = new Memory();
        Map<Integer, List<FunctionCall>> functions = createFunctionList("test! +++/ \n +++ >test +++");
        Interpreter interpreter = new Interpreter(got, instructions, null, null, null,
                new LoopChecker().check(instructions), functions, new Metrics());
        interpreter.run();
        Memory expected = new Memory();
        expected.getMemory()[0] = -125;
        expected.getMemory()[1] = -125;
        assertEquals(expected.display(), got.display());
    }

    @Test
    public void procedureInMacro()
            throws InputException, OutputException, SyntaxErrorException,
            ReadException, NotFoundException, IOException, LoopException,
            AsciiException, CellOverflowException, PointerException, OutOfMemory, BadArgumentsException {
        Memory got = new Memory();
        Map<Integer, List<FunctionCall>> functions = createFunctionList("test! +++/\n a? test++/\n a++a");
        Interpreter interpreter = new Interpreter(got, instructions, null, null, null,
                new LoopChecker().check(instructions), functions, new Metrics());
        interpreter.run();
        Memory expected = new Memory();
        expected.getMemory()[0] = -122;
        assertEquals(expected.display(), got.display());
    }

    @Test
    public void procedureWithInput()
            throws InputException, OutputException, SyntaxErrorException,
            ReadException, NotFoundException, IOException, LoopException,
            AsciiException, CellOverflowException, PointerException, OutOfMemory, BadArgumentsException {
        Memory got = new Memory();
        Map<Integer, List<FunctionCall>> functions = createFunctionList("test!\" +++>,/ \n +++ > +++ test\"t\" > test\"ntr\"test\"y\"");
        Interpreter interpreter = new Interpreter(got, instructions, null, null, null,
                new LoopChecker().check(instructions), functions, new Metrics());
        interpreter.run();
        Memory expected = new Memory();
        expected.getMemory()[0] = -125;
        expected.getMemory()[1] = -125;
        assertEquals(expected.display(), got.display());
    }

    @Test
    public void procedureWithLoop()
            throws InputException, OutputException, SyntaxErrorException,
            ReadException, NotFoundException, IOException, LoopException,
            AsciiException, CellOverflowException, PointerException, OutOfMemory, BadArgumentsException {
        Memory got = new Memory();
        Map<Integer, List<FunctionCall>> functions = createFunctionList("test! [->+<]>/ \n +++>+++test");
        Interpreter interpreter = new Interpreter(got, instructions, null, null, null,
                new LoopChecker().check(instructions), functions, new Metrics());
        interpreter.run();
        Memory expected = new Memory();
        expected.getMemory()[0] = -125;
        expected.getMemory()[1] = -125;
        assertEquals(expected.display(), got.display());
    }

    @Test
    public void simpleFunction()
            throws InputException, OutputException, SyntaxErrorException,
            ReadException, NotFoundException, IOException, LoopException,
            AsciiException, CellOverflowException, PointerException, OutOfMemory, BadArgumentsException {
        Memory got = new Memory();
        Map<Integer, List<FunctionCall>> functions = createFunctionList("test!$ +++/ \n test->test--");
        Interpreter interpreter = new Interpreter(got, instructions, null, null, null,
                new LoopChecker().check(instructions), functions, new Metrics());
        interpreter.run();
        Memory expected = new Memory();
        expected.getMemory()[0] = -126;
        expected.getMemory()[1] = -127;
        assertEquals(expected.display(), got.display());
    }

    @Test
    public void simpleFunctionOnly()
            throws InputException, OutputException, SyntaxErrorException,
            ReadException, NotFoundException, IOException, LoopException,
            AsciiException, CellOverflowException, PointerException, OutOfMemory, BadArgumentsException {
        Memory got = new Memory();
        Map<Integer, List<FunctionCall>> functions = createFunctionList("test!$ +++/ \n test");
        Interpreter interpreter = new Interpreter(got, instructions, null, null, null,
                new LoopChecker().check(instructions), functions, new Metrics());
        interpreter.run();
        Memory expected = new Memory();
        expected.getMemory()[0] = -125;
        assertEquals(expected.display(), got.display());
    }

    @Test
    public void simpleFunctionWithParameters()
            throws InputException, OutputException, SyntaxErrorException,
            ReadException, NotFoundException, IOException, LoopException,
            AsciiException, CellOverflowException, PointerException, OutOfMemory, BadArgumentsException {
        Memory got = new Memory();
        Map<Integer, List<FunctionCall>> functions = createFunctionList("a? +++ /\n" +
                " b? a*/ \n"   +
                " test!$ a*>b(%)*/ \n" +
                " test(1&3&2)");
        Interpreter interpreter = new Interpreter(got, instructions, null, null, null,
                new LoopChecker().check(instructions), functions, new Metrics());
        interpreter.run();
        Memory expected = new Memory();
        expected.getMemory()[0] = -110;
        assertEquals(expected.display(), got.display());
    }

    @Test
    public void functionInMacro()
            throws InputException, OutputException, SyntaxErrorException,
            ReadException, NotFoundException, IOException, LoopException,
            AsciiException, CellOverflowException, PointerException, OutOfMemory, BadArgumentsException {
        Memory got = new Memory();
        Map<Integer, List<FunctionCall>> functions = createFunctionList("test!$ +++/ \n a? ++test/ \n a--");
        Interpreter interpreter = new Interpreter(got, instructions, null, null, null,
                new LoopChecker().check(instructions), functions, new Metrics());
        interpreter.run();
        Memory expected = new Memory();
        expected.getMemory()[0] = -125;
        assertEquals(expected.display(), got.display());
    }

    @Test
    public void functionWithInput()
            throws InputException, OutputException, SyntaxErrorException,
            ReadException, NotFoundException, IOException, LoopException,
            AsciiException, CellOverflowException, PointerException, OutOfMemory, BadArgumentsException {
        Memory got = new Memory();
        Map<Integer, List<FunctionCall>> functions = createFunctionList("test!\"$ +++>,/ \n test\"abc\"");
        Interpreter interpreter = new Interpreter(got, instructions, null, null, null,
                new LoopChecker().check(instructions), functions, new Metrics());
        interpreter.run();
        Memory expected = new Memory();
        expected.getMemory()[0] = -128 + 'a';
        assertEquals(expected.display(), got.display());
    }

    @Test
    public void macroInFunction()
            throws InputException, OutputException, SyntaxErrorException,
            ReadException, NotFoundException, IOException, LoopException,
            AsciiException, CellOverflowException, PointerException, OutOfMemory, BadArgumentsException {
        Memory got = new Memory();
        Map<Integer, List<FunctionCall>> functions = createFunctionList("a? >+++/ \n test!$ ++>a /\n ++test");
        Interpreter interpreter = new Interpreter(got, instructions, null, null, null,
                new LoopChecker().check(instructions), functions, new Metrics());
        interpreter.run();
        Memory expected = new Memory();
        expected.getMemory()[0] = -125;
        assertEquals(expected.display(), got.display());
    }

    @Test
    public void procedureInFunction()
            throws InputException, OutputException, SyntaxErrorException,
            ReadException, NotFoundException, IOException, LoopException,
            AsciiException, CellOverflowException, PointerException, OutOfMemory, BadArgumentsException {
        Memory got = new Memory();
        Map<Integer, List<FunctionCall>> functions = createFunctionList("a! +++/\n b!$ a>+++/ \n a>b");
        Interpreter interpreter = new Interpreter(got, instructions, null, null, null,
                new LoopChecker().check(instructions), functions, new Metrics());
        interpreter.run();
        Memory expected = new Memory();
        expected.getMemory()[1] = -125;
        assertEquals(expected.display(), got.display());
    }

    @Test
    public void functionInFunction()
            throws InputException, OutputException, SyntaxErrorException,
            ReadException, NotFoundException, IOException, LoopException,
            AsciiException, CellOverflowException, PointerException, OutOfMemory, BadArgumentsException {
        Memory got = new Memory();
        Map<Integer, List<FunctionCall>> functions = createFunctionList("a!$ +++/\n b!$ a>++ /\n a>b");
        Interpreter interpreter = new Interpreter(got, instructions, null, null, null,
                new LoopChecker().check(instructions), functions, new Metrics());
        interpreter.run();
        Memory expected = new Memory();
        expected.getMemory()[0] = -125;
        expected.getMemory()[1] = -126;
        assertEquals(expected.display(), got.display());
    }

    @Test
    public void functionWithLoop()
            throws InputException, OutputException, SyntaxErrorException,
            ReadException, NotFoundException, IOException, LoopException,
            AsciiException, CellOverflowException, PointerException, OutOfMemory, BadArgumentsException {
        Memory got = new Memory();
        Map<Integer, List<FunctionCall>> functions = createFunctionList("test!$ [->+<]>++/ \n  \n ++>++test>++");
        Interpreter interpreter = new Interpreter(got, instructions, null, null, null,
                new LoopChecker().check(instructions), functions, new Metrics());
        interpreter.run();
        Memory expected = new Memory();
        expected.getMemory()[0] = -126;
        expected.getMemory()[1] = -124;
        expected.getMemory()[2] = -126;
        assertEquals(expected.display(), got.display());
        /*
        Il semblerait qu'il y ait un problème dans les boucles, la boucle décrémente la case jusqu'à la valeur -127, au
        lieu d'aller jusqu'à -128, ce qui fait qu'il y a une différence de 1 entre le expected et le got. De mémoire il
        me semble bien que l'on sort d'une boucle avant d'atteindre le out of bound non ?
         */
    }

    @Test
    public void functionRecursive()
            throws InputException, OutputException, SyntaxErrorException,
            ReadException, NotFoundException, IOException, LoopException,
            AsciiException, CellOverflowException, PointerException, OutOfMemory, BadArgumentsException {
        Memory got = new Memory();
        Map<Integer, List<FunctionCall>> functions = createFunctionList("decr!$ -[decr]/\n" +
                "+++decr");
        Interpreter interpreter = new Interpreter(got, instructions, null, null, null,
                new LoopChecker().check(instructions), functions, new Metrics());
        interpreter.run();
        assertEquals("", got.display());
    }

}
