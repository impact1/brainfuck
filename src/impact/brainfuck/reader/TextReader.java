package impact.brainfuck.reader;


import impact.brainfuck.exception.BadArgumentsException;
import impact.brainfuck.exception.NotFoundException;
import impact.brainfuck.exception.ReadException;
import impact.brainfuck.exception.SyntaxErrorException;
import impact.brainfuck.macro.DefinedElements;
import impact.brainfuck.macro.Function;
import impact.brainfuck.macro.FunctionCall;
import impact.brainfuck.macro.Macro;
import impact.brainfuck.operation.Operation;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Jeremy Lara, Florian Bourniquel, Guillaume André
 * @version 1.0
 *          <p>
 *          This class read a text file as program input
 *          and send the instructions read to the interpreter.
 */

public class TextReader implements Reader {

    private static final Map<String, String> trad;
    public static int lineCount = 1;

    static {
        trad = new HashMap<>();
        trad.put("+", "INCR");
        trad.put("-", "DECR");
        trad.put("<", "LEFT");
        trad.put(">", "RIGHT");
        trad.put(".", "OUT");
        trad.put(",", "IN");
        trad.put("[", "JUMP");
        trad.put("]", "BACK");
    }

    public boolean inMacro = false;
    private BufferedReader br;
    private DefinedElements definedElements;
    private Map<Integer, List<FunctionCall>> functions;
    private List<Operation> instructions;

    /**
     * Standard constructor.
     *
     * @param file - the brainfuck program file
     * @throws NotFoundException - if the file could not be found
     */
    public TextReader(File file) throws NotFoundException {
        definedElements = new DefinedElements();
        try {
            InputStream is = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(is);
            br = new BufferedReader(isr);
        } catch (FileNotFoundException e) {
            throw new NotFoundException(file.getName());
        }
        functions = new HashMap<>();
        instructions = new ArrayList<>();
    }

    /**
     * Translates the program file into its corresponding list of instructions.
     *
     * @return the corresponding list of operations
     * @throws ReadException        - if the program file could not be read
     * @throws SyntaxErrorException - if the program file syntax is incorrect
     */
    public List<Operation> read() throws ReadException, SyntaxErrorException, BadArgumentsException {
        try {
            String line;
            while ((line = br.readLine()) != null) {
                line = removeComment(line);
                if (line.contains("?") || line.contains("!")) {
                    defineMacro(line);
                } else {
                    parseLine(line);
                }
                lineCount++;
            }
            return instructions;
        } catch (IOException e) {
            throw new ReadException();
        }
    }

    public Map<Integer, List<FunctionCall>> getFunctions() {
        return functions;
    }

    /**
     * Removes comments from brainfuck code.
     *
     * @param line - the String to be cleared
     * @return the cleaned String
     */
    public String removeComment(String line) {
        if (line.contains("#")) {
            line = line.substring(0, line.indexOf("#"));
        }
        return line;
    }

    /**
     * Defines a macro or a function and adds it to the list of defined elements.
     *
     * @param line - the line containing the element definition
     * @throws SyntaxErrorException - if the element definition was incorrect
     * @throws IOException          - if the brainfuck code could not be read
     */
    public void defineMacro(String line) throws SyntaxErrorException, IOException, BadArgumentsException {
        Macro currentElement;
        if (line.contains("?")) {
            currentElement = new Macro(definedElements, line, functions);
        } else {
            currentElement = new Function(definedElements, line, functions);
        }
        while (!line.contains("/")) {
            line = br.readLine();
            if (line == null) {
                throw new SyntaxErrorException("Brainfuck code ends on incomplete element definition (missing \"/\").");
            }
            lineCount++;
            currentElement.addLine(line);
        }
        definedElements.add(currentElement);
        definedElements.setRecursive(null);
    }

    /**
     * Translates a String of brainfuck code into its corresponding list of operations.
     * Comments must be removed beforehand.
     *
     * @param line - the String to be parsed
     * @throws SyntaxErrorException - if the line syntax is incorrect
     */
    public void parseLine(String line) throws SyntaxErrorException {
        line = line.trim();
        if (checkForLongSyntax(line)) {
            return;
        }
        Pattern groupName = Pattern.compile("[0-9a-z_]+([0-9()&]+)*(\\*[0-9]*)*(\"[^\"]*\")*|[+\\-><.,\\[\\]]+");
        Matcher matcher = groupName.matcher(line);
        while (matcher.find()) {
            char first = matcher.group().charAt(0);
            if (Character.isLowerCase(first) || Character.isDigit(first) || first == '_') {
                addElementInstructions(matcher.group());
            } else {
                instructions.addAll(parseSymbols(matcher.group()));
            }
            line = matcher.replaceFirst("");
            matcher = groupName.matcher(line);
        }
        checkForExtrasCharacters(line);
    }

    /**
     * Searches a given line for long brainfuck syntax (INCR, DECR).
     *
     * @param line - the line to analyze
     * @return true if a long instruction was found and successfully parsed, false otherwise
     * @throws SyntaxErrorException - if a long instruction was found, but is unknown
     */
    public boolean checkForLongSyntax(String line) throws SyntaxErrorException {
        Pattern longSyntax = Pattern.compile("^[A-Z]+$");
        Matcher matcher = longSyntax.matcher(line);
        if (matcher.find()) {
            for (Operation o : Operation.values()) {
                if (line.equals(o.name())) {
                    instructions.add(o);
                    return true;
                }
            }
            throw new SyntaxErrorException("Instruction \"" + matcher.group() + "\" is not a valid long brainfuck instruction.");
        }
        return false;
    }

    /**
     * Adds the instructions of a macro/function.
     * In he case of a function, the corresponding FunctionCall will be created and no instructions will be added.
     * Extracts the inputs used and the parameters before passing them to the corresponding element to get its brainfuck instructions.
     * Repeats this macro instructions (and function calls) if the token for repetition (*) was used.
     *
     * @param group - the group containing he name of the element, its parameters, its inputs and the number of time it should be repeated
     * @throws SyntaxErrorException - if the given String contained incorrect syntax
     */
    public void addElementInstructions(String group) throws SyntaxErrorException {
        Pattern macroName = Pattern.compile("^[a-z0-9_]+");
        Matcher nameMatcher = macroName.matcher(group);
        Macro macro;
        if (nameMatcher.find()) {
            macro = definedElements.getMacro(nameMatcher.group());
            if (macro == null) {
                throw new SyntaxErrorException("Element \"" + nameMatcher.group() + "\" is not defined.");
            }
            group = nameMatcher.replaceFirst("");
        } else {
            throw new SyntaxErrorException("Illegal element operation: \"" + group + "\".");
        }
        int repeats = 1;
        List<Integer> macroParams = new ArrayList<>();
        Pattern getRepeats = Pattern.compile("\\*[0-9]+");
        Matcher repeatMatcher = getRepeats.matcher(group);
        if (repeatMatcher.find()) {
            repeats = Integer.parseInt(repeatMatcher.group().substring(1));
            group = repeatMatcher.replaceFirst("");
        }
        List<String> inputs = new ArrayList<>();
        Pattern findInput = Pattern.compile("\"[^\"]+\"");
        Matcher inputMatcher = findInput.matcher(group);
        if (inputMatcher.find()) {
            inputs = parseInputs(inputMatcher.group());
            group = inputMatcher.replaceFirst("");
        }
        if (inputs.size() != macro.getInputCount()) {
            throw new SyntaxErrorException("Element \"" + macro.toString() + "\" was called with " + inputs.size() +
                    " inputs instead of " + macro.getInputCount() + ".");
        }
        Pattern getParams = Pattern.compile("\\([0-9&]+\\)");
        Matcher paramMatcher = getParams.matcher(group);
        if (paramMatcher.find()) {
            macroParams.addAll(parseParams(paramMatcher.group()));
            group = paramMatcher.replaceFirst("");
        }
        checkForExtrasCharacters(group);
        if (macroParams.size() != macro.getParamCount()) {
            throw new SyntaxErrorException("Element \"" + macro.toString() + "\" was called with " + macroParams.size() + " instead of " +
                    macro.getParamCount() + ".");
        }
        List<Operation> toRepeat = macro.toList(macroParams, inputs, instructions.size(), functions, false);
        Map<Integer, List<FunctionCall>> callsToRepeat = new HashMap<>();
        for (int call : functions.keySet()) {
            if (call >= instructions.size()) {
                callsToRepeat.put(call - instructions.size(), new ArrayList<>());
                callsToRepeat.get(call - instructions.size()).addAll(functions.get(call));
            }
        }
        int sizeBefore = instructions.size();
        for (int i = 0; i < repeats; i++) {
            instructions.addAll(toRepeat);
            if (!callsToRepeat.isEmpty() && i > 0) {
                for (int call : callsToRepeat.keySet()) {
                    functions.putIfAbsent(sizeBefore + toRepeat.size() * i + call, new ArrayList<>());
                    functions.get(sizeBefore + toRepeat.size() * i + call).addAll(callsToRepeat.get(call));
                }
            }
        }
    }

    /**
     * Translates a String of brainfuck symbols (short syntax) into their corresponding operations.
     *
     * @param line - the String to be parsed
     * @return a list of all the operations in order of apparition
     * @throws SyntaxErrorException - if the line contains a non-brainfuck symbol
     */
    public static List<Operation> parseSymbols(String line) throws SyntaxErrorException {
        List<Operation> result = new ArrayList<>();
        for (int i = 0; i < line.length(); i++) {
            try {
                if (line.charAt(i) != ' ' && line.charAt(i) != '\t') {
                    result.add(Operation.valueOf(trad.get(String.valueOf(line.charAt(i)))));
                }
            } catch (IllegalArgumentException | NullPointerException e) {
                throw new SyntaxErrorException(line.charAt(i));
            }
        }
        return result;
    }

    /**
     * Checks for extra non-whitespace characters in the given String.
     *
     * @param line - the String to analyze
     * @throws SyntaxErrorException - if non-whitespace characters were found
     */
    public void checkForExtrasCharacters(String line) throws SyntaxErrorException {
        line = line.replaceAll("\\s", "");
        if (line.equals("/")) {
            throw new SyntaxErrorException("Found end of element character (\"/\") outside of element definition.");
        }
        if (!line.equals("")) {
            throw new SyntaxErrorException("Invalid characters or syntax: \"" + line + "\".");
        }
    }

    /**
     * Transforms properly formatted element inputs into their corresponding list of inputs.
     * Syntax must be checked beforehand.
     *
     * @param expression - the String to be parsed
     * @return a list containing all of the inputs in order of apparition
     */
    public List<String> parseInputs(String expression) {
        Pattern findInputs = Pattern.compile("[^&\"]+");
        Matcher matcher = findInputs.matcher(expression);
        List<String> result = new ArrayList<>();
        while (matcher.find()) {
            if (matcher.group().equals("%")) {
                result.add("");
            } else {
                result.add(matcher.group());
            }
        }
        return result;
    }

    /**
     * Transforms a properly formatted element parameter String into its corresponding list of integers.
     * Syntax must be checked beforehand.
     *
     * @param line - the String to be parsed
     * @return a list containing all of the parameters in order of apparition
     */
    public List<Integer> parseParams(String line) {
        List<Integer> result = new ArrayList<>();
        Pattern param = Pattern.compile("[0-9]+");
        Matcher matcher = param.matcher(line);
        while (matcher.find()) {
            result.add(Integer.parseInt(matcher.group()));
            line = matcher.replaceFirst("");
            matcher = param.matcher(line);
        }
        return result;
    }


}

