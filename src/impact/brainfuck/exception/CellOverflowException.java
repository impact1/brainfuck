package impact.brainfuck.exception;

/**
 * Thrown when the value in a memory cell goes out of bounds, [0, 255]. Message
 * displays cell, instruction and overflow type.
 * 
 * @author Guillaume Andre, Elliot Kauffmann
 * @version 2.0
 */
public class CellOverflowException extends Exception {

	private static final long serialVersionUID = -5991594281133806177L;
	public static int minLimit = 0;
	public static int maxLimit = 29999;

	/**
	 * @param cell - the memory cell that went out of bounds
	 * @param value - the value of the cell before going out of bounds
	 * @param instruction - instruction at which the exception was thrown
	 */
	public CellOverflowException(int cell, int value, int instruction) {
		super("Cell overflow at cell " + cell + " (tried to reach beyond " + value + "), at instruction " + instruction + ".");
		System.out.println(this.getMessage());
		System.exit(1);
	}

}
