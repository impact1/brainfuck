package impact.brainfuck.operation;

import impact.brainfuck.memory.Memory;
import impact.brainfuck.operation.OperationINCR;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import static org.junit.Assert.assertEquals;

/**
 * @author Florian Bourniquel
 * @version 1.0
 * Test for OperationINCR
 */
public class OperationINCRTest {

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void doOperation() throws Exception {
        Memory memory = new Memory();
        OperationINCR incr = new OperationINCR();
        incr.doOperation(memory,0);
        assertEquals("C0: 1\n",memory.display());

    }


    @Test //(expected = CellOverflowException.class)
    public void cellOverFlowException () throws Exception {
        Memory memory = new Memory();
        OperationINCR incr = new OperationINCR();
        for (int i = 0 ; i < 255 ; i++) {
            incr.doOperation(memory,0);
        }
        assertEquals("C0: 255\n",memory.display());
        exit.expectSystemExitWithStatus(1);
        incr.doOperation(memory,0);
    }

}