#!/bin/bash
# Script "CreateBfck"

mkdir ./bin
javac -cp ./src ./src/impact/brainfuck/main/Main.java -d ./bin
jar cmf MANIFEST.MF bfck.jar -C bin/ .
rm -rf ./bin
