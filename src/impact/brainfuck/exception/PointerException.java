package impact.brainfuck.exception;

/**
 * Thrown when the memory pointer goes out of bounds.
 * 
 * @author Guillaume Andre, Elliot Kauffmann
 * @version 2.0
 */
public class PointerException extends Exception {

	private static final long serialVersionUID = 9126982692403977545L;

	/**
	 * @param pointer - position of the memory pointer
	 * @param instruction - position of the instruction index
	 */
	public PointerException(short pointer, int instruction) {
		super("Pointer tried to reach beyond cell " + pointer + ", at instruction " + instruction + ".");
		System.out.println(this.getMessage());
		System.exit(2);
	}
}
