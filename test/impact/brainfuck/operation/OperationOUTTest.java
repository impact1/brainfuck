package impact.brainfuck.operation;

import impact.brainfuck.check.LoopChecker;
import impact.brainfuck.exception.CellOverflowException;
import impact.brainfuck.exception.OutputException;
import impact.brainfuck.interpreter.Interpreter;
import impact.brainfuck.main.Metrics;
import impact.brainfuck.memory.Memory;
import impact.brainfuck.operation.Operation;
import impact.brainfuck.operation.OperationINCR;
import impact.brainfuck.operation.OperationOUT;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

/**
 * @author Guillaume Andre, Elliot Kauffmann
 * @version 2.1
 * 				Test for OperationOUT
 *
 *          if this throws NullPointerException on all 3 tests, that's because you're using IntelliJ.
 *          Seriously though, it comes from a bug with System.out and System.in, and is an IDE bug.
 *          Don't worry too much about it.
 *          Running this test class alone should give correct results.
 */
public class OperationOUTTest {

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@Before
	public void setUp() {

	}

	@After
	public void tearDown() {

	}

	@Test
	public void OutTest() throws OutputException, CellOverflowException {
		Memory m = new Memory();
		OperationOUT o = new OperationOUT();
		OperationINCR oi = new OperationINCR();
		for (int i = 0; i < 97; i++) {
			oi.doOperation(m, 0);
		}
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		PrintStream p = new PrintStream(bo);
		OperationOUT.setOutStream(p);
		o.doOperation(m, 0);
		assertEquals("a", bo.toString());
	}

	@Test
	public void writeOut() throws Exception {
		Memory m = new Memory();
		OperationINCR oi = new OperationINCR();
		for (int i = 0; i < 97; i++) {
			oi.doOperation(m, 0);
		}
		ArrayList<Operation> inst = new ArrayList<>();
		inst.add(Operation.OUT);
		File out = folder.newFile();
		Interpreter i = new Interpreter(m, inst, null, new PrintStream(out), null, new LoopChecker().check(inst),new HashMap<>(), new Metrics());
		//NullPointerException ? see above.
		i.run();
		Scanner sc = new Scanner(out);
		String outCheck = sc.nextLine();
		sc.close();
		assertEquals("a", outCheck);
	}
}
