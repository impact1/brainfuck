package impact.brainfuck.reader;


import impact.brainfuck.exception.BadArgumentsException;
import impact.brainfuck.exception.NotFoundException;
import impact.brainfuck.exception.ReadException;
import impact.brainfuck.exception.SyntaxErrorException;
import impact.brainfuck.operation.Operation;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.rules.TemporaryFolder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


/**
 * @author Jeremy Lara
 * @version 1.0
 * Test for ImageReader
 */
public class ImageReaderTest {

    @Rule
    public final TemporaryFolder temp = new TemporaryFolder();

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test //(expected = NotFoundException.class)
    public void NotFoundException() throws Exception {
        File file = new File("/Users/hrouzghzu.bmp"); //nom de fichier impossible
        Reader reader = new ImageReader(file);
        exit.expectSystemExitWithStatus(3);
        reader.read();
    }

    @Test
    public void emptyImage() throws IOException, NotFoundException, SyntaxErrorException, ReadException, BadArgumentsException {
        List<Operation> list = new ArrayList<>();
        File file = temp.newFile("emptyImage.bmp");
        Reader reader = new ImageReader(file.getAbsoluteFile());
        assertEquals(list, reader.read());
    }

    @Test
    public void read() throws Exception {
        // building input list
        List<Operation> list = new ArrayList<>();
        list.add(Operation.INCR);
        list.add(Operation.DECR);
        list.add(Operation.JUMP);
        list.add(Operation.BACK);
        list.add(Operation.LEFT);
        list.add(Operation.RIGHT);
        list.add(Operation.IN);
        list.add(Operation.OUT);
        // drawing image
        BufferedImage image = new BufferedImage(9, 9, 5);
        for(int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++)
                image.setRGB(x, y, 0xFFFFFF);
        }
        for(int x = 3; x < 6; x++) {
            for (int y = 0; y < 3; y++)
                image.setRGB(x, y, 0x4B0082);
        }
        for(int x = 6; x < 9; x++) {
            for (int y = 0; y < 3; y++)
                image.setRGB(x, y, 0xFF7F00);
        }
        for(int x = 0; x < 3; x++) {
            for (int y = 3; y < 6; y++)
                image.setRGB(x, y, 0xFF0000);
        }
        for(int x = 3; x < 6; x++) {
            for (int y = 3; y < 6; y++)
                image.setRGB(x, y, 0x9400D3);
        }
        for(int x = 6; x < 9; x++) {
            for (int y = 3; y < 6; y++)
                image.setRGB(x, y, 0x0000FF);
        }
        for(int x = 0; x < 3; x++) {
            for (int y = 6; y < 9; y++)
                image.setRGB(x, y, 0xFFFF00);
        }
        for(int x = 3; x < 6; x++) {
            for (int y = 6; y < 9; y++)
                image.setRGB(x, y, 0x00FF00);
        }
        File file =  temp.newFile("image.bmp");
        ImageIO.write(image, "bmp", new File(String.valueOf(file)));
        Reader reader = new ImageReader(file.getAbsoluteFile());
        assertEquals(list, reader.read());
    }

}

