package impact.brainfuck.operation;

import impact.brainfuck.memory.Memory;
import impact.brainfuck.operation.OperationDECR;
import impact.brainfuck.operation.OperationINCR;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import static org.junit.Assert.assertEquals;

/**
 * @author Florian Bourniquel
 * @version 1.0
 * Test for OperationDECR
 */
public class OperationDECRTest {

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void doOperation() throws Exception {
        Memory memory = new Memory();
        OperationINCR incr = new OperationINCR();
        OperationDECR decr = new OperationDECR();
        incr.doOperation(memory,0);
        incr.doOperation(memory,0);
        incr.doOperation(memory,0);
        decr.doOperation(memory,0);
        assertEquals("C0: 2\n",memory.display());
    }

    @Test //(expected = CellOverflowException.class)
    public void cellOverFlowException () throws Exception {
        Memory memory = new Memory();
        OperationDECR decr = new OperationDECR();
        exit.expectSystemExitWithStatus(1);
        decr.doOperation(memory,0);
    }

}