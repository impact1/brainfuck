package impact.brainfuck.generator;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import impact.brainfuck.operation.Operation;

/**
 * This class can generate functional C code from the instruction list provided
 * by the reader.
 * 
 * @author Elliot Kauffmann
 * @version 1.2
 */
public class CGenerator {

	/** Map that links instruction enumeration instances to C code strings */
	private Map<Operation, String> instructionsToC;

	/**
	 * This method takes an empty StringBuilder and appends it with the
	 * beginning of a C program capable of executing BrainF*ck instructions.
	 * 
	 * @param out - the StringBuilder to be initialized with C code
	 */
	private void initializeStringBuilder(StringBuilder out) {
		out.append("#include <stdio.h>").append('\n').append("#include <stdlib.h>").append('\n').append('\n').append("typedef unsigned char Byte;")
				.append('\n').append('\n').append("int main() {").append('\n').append('\n').append("    Byte* p = malloc(30000 * sizeof(Byte));")
				.append('\n').append('\n').append("    // Code generation from BrainF*ck to C begins here").append('\n');
	}

	/**
	 * This method takes a StringBuilder containing a C program capable of
	 * executing BrainF*ck instructions that needs to be ended and adds the end
	 * to it.
	 * 
	 * @param out - the StringBuilder containing a C program to be ended.
	 */
	private void endStringBuiler(StringBuilder out) {
		out.append("    // Code generation ends here").append('\n').append('\n').append("    return 0;").append('\n').append("}");
	}

	/**
	 * This constructor builds a new CGenerator object and inserts the
	 * translations of BrainF*ck instructions in C code in its map.
	 */
	public CGenerator() {
		instructionsToC = new HashMap<>();
		instructionsToC.put(Operation.INCR, "(*p)++;");
		instructionsToC.put(Operation.DECR, "(*p)--;");
		instructionsToC.put(Operation.LEFT, "p--;");
		instructionsToC.put(Operation.RIGHT, "p++;");
		instructionsToC.put(Operation.IN, "(*p) = getchar();");
		instructionsToC.put(Operation.OUT, "putchar(*p);");
		instructionsToC.put(Operation.JUMP, "while (*p) {");
		instructionsToC.put(Operation.BACK, "}");
	}

	/**
	 * This method takes an initialized StringBuilder and appends it with the
	 * translations in C code of the instructions contained in the List.
	 * 
	 * @param instructions - the List of BrainF*ck instructions to be translated
	 * @param output - the Output file or stream
	 */
	public void generate(List<Operation> instructions, PrintStream output) {

		StringBuilder out = new StringBuilder();
		initializeStringBuilder(out);
		int indent = 1;

		for (Operation currentOperation : instructions) {
			if (currentOperation == Operation.BACK)
				indent--;
			for (int i = 0; i < indent; i++)
				out.append("    ");
			out.append(instructionsToC.get(currentOperation)).append('\n');
			if (currentOperation == Operation.JUMP)
				indent++;
		}

		endStringBuiler(out);
		output.println(out.toString());
	}

}
