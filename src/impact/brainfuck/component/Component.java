package impact.brainfuck.component;

import impact.brainfuck.exception.SyntaxErrorException;
import impact.brainfuck.macro.FunctionCall;
import impact.brainfuck.operation.Operation;

import java.util.List;
import java.util.Map;

/**
 * @author Guillaume Andre
 *         <p>
 *         Interface for the components of a macro/function (= element).
 */
public interface Component {

    /**
     * Translates this component into its corresponding brainfuck operations.
     *
     * @param params           - the parameters acquired in brainfuck code
     * @param instructionsSize - the number of instructions read before - used to properly position function calls
     * @param functions        - the map where functions calls should be inserted
     * @param inputs           - the inputs acquired in brainfuck code
     * @return the list of instructions of this component
     * @throws SyntaxErrorException - if not enough parameters were used
     */
    List<Operation> componentToList(List<Integer> params, int instructionsSize, Map<Integer, List<FunctionCall>> functions, List<String> inputs) throws SyntaxErrorException;

    /**
     * Gives the updated parameters list after componentToList() was used.
     *
     * @return the parameters list after the required parameters were extracted
     */
    List<Integer> getParametersAfterToList();

    /**
     * Gives the updated input list after componentToList() was used.
     *
     * @return the input list after the required parameters were extracted
     */
    List<String> getInputsAfterToList();
}
