package impact.brainfuck.operation;

/**
 * @author Florian Bourniquel
 * @version 1.0
 *          <p>
 *          Enum of the operation.
 */
public enum Operation {
    INCR(new OperationINCR()),
    DECR(new OperationDECR()),
    LEFT(new OperationLEFT()),
    RIGHT(new OperationRIGHT()),
    IN(new OperationIN()),
    OUT(new OperationOUT()),
    JUMP(new OperationJUMP()),
    BACK(new OperationBACK());

    CommandOperation operation;

    Operation(CommandOperation operation) {
        this.operation = operation;
    }

    public CommandOperation getOperation() {
        return operation;
    }

}
