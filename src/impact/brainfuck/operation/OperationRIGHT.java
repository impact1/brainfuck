package impact.brainfuck.operation;

import impact.brainfuck.exception.CellOverflowException;
import impact.brainfuck.exception.PointerException;
import impact.brainfuck.memory.Memory;

/**
 * @author Florian Bourniquel, Guillaume Andre
 * @version 1.0
 *          <p>
 *          Implementation of the Interface CommandOperation.
 *          This class moves the memory pointer to the right.
 */
public class OperationRIGHT implements CommandOperation {

    /**
     * Moves the memory pointer 1 cell to the right.
     *
     * @param memory                  - the current memory of the brainfuck program.
     * @param currentInstructionIndex - the instruction pointer
     * @throws PointerException - if the pointer tries to go above 29999
     */
    @Override
    public int doOperation(Memory memory, int currentInstructionIndex) throws PointerException {
        if (memory.getCurrentCellIndex() == CellOverflowException.maxLimit) {
            throw new PointerException(memory.getCurrentCellIndex(), currentInstructionIndex);
        }
        memory.setMemoryPointer((short) (memory.getCurrentCellIndex() + 1));
        return currentInstructionIndex + 1;
    }
}
