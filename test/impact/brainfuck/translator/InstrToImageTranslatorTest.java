package impact.brainfuck.translator;

import impact.brainfuck.operation.Operation;
import impact.brainfuck.translator.InstrToImageTranslator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Unit test for text to image translator. Tests each instruction one by one.
 * Then tests with a program of 8 instructions, one of each.
 * 
 * @author Elliot Kauffmann
 * @version 1.2
 */
public class InstrToImageTranslatorTest {

	/** the instruction translator **/
	private InstrToImageTranslator instrToImageTranslator;
	/** reference imge **/
	private BufferedImage image;
	/** list of instructions to translate **/
	private List<Operation> list;
	/** temporary test file to draw into **/
	private File instrToImageTestFile;

	/**
	 * Initialize the translator, test file, and the instruction list.
	 */
	@Before
	public void setUp() throws Exception {
		instrToImageTranslator = new InstrToImageTranslator();
		instrToImageTestFile = new File("instrToImageTestFile.bmp");
		instrToImageTestFile.deleteOnExit();
		System.setOut(new PrintStream(instrToImageTestFile));
		list = new ArrayList<>();
	}

	/**
	 * Clean everything up once the test is done.
	 */
	@After
	public void tearDown() {
		instrToImageTestFile.delete();
		System.out.close();
	}

	/**
	 * The next 8 methods test whether the translator is capable of drawing each
	 * individual instruction.
	 */

	@Test
	public void translateINCR() throws Exception {

		list.add(Operation.INCR);
		image = new BufferedImage(3, 3, 5);
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++)
				image.setRGB(x, y, 0xFFFFFF);
		}
		instrToImageTranslator.translate(list, System.out);
		BufferedImage translation = ImageIO.read(instrToImageTestFile);
		for (int y = 0; y < Math.max(image.getHeight(), translation.getHeight()); y++) {
			for (int x = 0; x < Math.max(image.getWidth(), translation.getWidth()); x++) {
				assertEquals(image.getRGB(x, y), translation.getRGB(x, y));
			}
		}
	}

	@Test
	public void translateDECR() throws Exception {

		list.add(Operation.DECR);
		image = new BufferedImage(3, 3, 5);
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++)
				image.setRGB(x, y, 0x4B0082);
		}
		instrToImageTranslator.translate(list, System.out);
		BufferedImage translation = ImageIO.read(instrToImageTestFile);
		for (int y = 0; y < Math.max(image.getHeight(), translation.getHeight()); y++) {
			for (int x = 0; x < Math.max(image.getWidth(), translation.getWidth()); x++) {
				assertEquals(image.getRGB(x, y), translation.getRGB(x, y));
			}
		}
	}

	@Test
	public void translateJUMP() throws Exception {

		list.add(Operation.JUMP);
		image = new BufferedImage(3, 3, 5);
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++)
				image.setRGB(x, y, 0xFF7F00);
		}
		instrToImageTranslator.translate(list, System.out);
		BufferedImage translation = ImageIO.read(instrToImageTestFile);
		for (int y = 0; y < Math.max(image.getHeight(), translation.getHeight()); y++) {
			for (int x = 0; x < Math.max(image.getWidth(), translation.getWidth()); x++) {
				assertEquals(image.getRGB(x, y), translation.getRGB(x, y));
			}
		}
	}

	@Test
	public void translateBACK() throws Exception {

		list.add(Operation.BACK);
		image = new BufferedImage(3, 3, 5);
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++)
				image.setRGB(x, y, 0xFF0000);
		}
		instrToImageTranslator.translate(list, System.out);
		BufferedImage translation = ImageIO.read(instrToImageTestFile);
		for (int y = 0; y < Math.max(image.getHeight(), translation.getHeight()); y++) {
			for (int x = 0; x < Math.max(image.getWidth(), translation.getWidth()); x++) {
				assertEquals(image.getRGB(x, y), translation.getRGB(x, y));
			}
		}
	}

	@Test
	public void translateLEFT() throws Exception {

		list.add(Operation.LEFT);
		image = new BufferedImage(3, 3, 5);
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++)
				image.setRGB(x, y, 0x9400D3);
		}
		instrToImageTranslator.translate(list, System.out);
		BufferedImage translation = ImageIO.read(instrToImageTestFile);
		for (int y = 0; y < Math.max(image.getHeight(), translation.getHeight()); y++) {
			for (int x = 0; x < Math.max(image.getWidth(), translation.getWidth()); x++) {
				assertEquals(image.getRGB(x, y), translation.getRGB(x, y));
			}
		}
	}

	@Test
	public void translateRIGHT() throws Exception {

		list.add(Operation.RIGHT);
		image = new BufferedImage(3, 3, 5);
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++)
				image.setRGB(x, y, 0x0000FF);
		}
		instrToImageTranslator.translate(list, System.out);
		BufferedImage translation = ImageIO.read(instrToImageTestFile);
		for (int y = 0; y < Math.max(image.getHeight(), translation.getHeight()); y++) {
			for (int x = 0; x < Math.max(image.getWidth(), translation.getWidth()); x++) {
				assertEquals(image.getRGB(x, y), translation.getRGB(x, y));
			}
		}
	}

	@Test
	public void translateIN() throws Exception {

		list.add(Operation.IN);
		image = new BufferedImage(3, 3, 5);
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++)
				image.setRGB(x, y, 0xFFFF00);
		}
		instrToImageTranslator.translate(list, System.out);
		BufferedImage translation = ImageIO.read(instrToImageTestFile);
		for (int y = 0; y < Math.max(image.getHeight(), translation.getHeight()); y++) {
			for (int x = 0; x < Math.max(image.getWidth(), translation.getWidth()); x++) {
				assertEquals(image.getRGB(x, y), translation.getRGB(x, y));
			}
		}
	}

	@Test
	public void translateOUT() throws Exception {

		list.add(Operation.OUT);
		image = new BufferedImage(3, 3, 5);
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++)
				image.setRGB(x, y, 0x00FF00);
		}
		instrToImageTranslator.translate(list, System.out);
		BufferedImage translation = ImageIO.read(instrToImageTestFile);
		for (int y = 0; y < Math.max(image.getHeight(), translation.getHeight()); y++) {
			for (int x = 0; x < Math.max(image.getWidth(), translation.getWidth()); x++) {
				assertEquals(image.getRGB(x, y), translation.getRGB(x, y));
			}
		}
	}

	/**
	 * This method tests whether the translator is capable of drawing all the
	 * instructions in the correct order.
	 */
	@Test
	public void translateAll() throws Exception {

		list.add(Operation.INCR);
		list.add(Operation.DECR);
		list.add(Operation.JUMP);
		list.add(Operation.BACK);
		list.add(Operation.LEFT);
		list.add(Operation.RIGHT);
		list.add(Operation.IN);
		list.add(Operation.OUT);
		image = new BufferedImage(9, 9, 5);
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++)
				image.setRGB(x, y, 0xFFFFFF);
		}
		for (int x = 3; x < 6; x++) {
			for (int y = 0; y < 3; y++)
				image.setRGB(x, y, 0x4B0082);
		}
		for (int x = 6; x < 9; x++) {
			for (int y = 0; y < 3; y++)
				image.setRGB(x, y, 0xFF7F00);
		}
		for (int x = 0; x < 3; x++) {
			for (int y = 3; y < 6; y++)
				image.setRGB(x, y, 0xFF0000);
		}
		for (int x = 3; x < 6; x++) {
			for (int y = 3; y < 6; y++)
				image.setRGB(x, y, 0x9400D3);
		}
		for (int x = 6; x < 9; x++) {
			for (int y = 3; y < 6; y++)
				image.setRGB(x, y, 0x0000FF);
		}
		for (int x = 0; x < 3; x++) {
			for (int y = 6; y < 9; y++)
				image.setRGB(x, y, 0xFFFF00);
		}
		for (int x = 3; x < 6; x++) {
			for (int y = 6; y < 9; y++)
				image.setRGB(x, y, 0x00FF00);
		}
		instrToImageTranslator.translate(list, System.out);
		BufferedImage translation = ImageIO.read(instrToImageTestFile);
		for (int y = 0; y < Math.max(image.getHeight(), translation.getHeight()); y++) {
			for (int x = 0; x < Math.max(image.getWidth(), translation.getWidth()); x++) {
				assertEquals(image.getRGB(x, y), translation.getRGB(x, y));
			}
		}
	}
}