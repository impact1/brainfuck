package impact.brainfuck.main;

import impact.brainfuck.exception.BadArgumentsException;
import impact.brainfuck.exception.NotFoundException;
import impact.brainfuck.macro.Function;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Guillaume Andre, Jeremy Lara
 * @version 2.0
 *          <p>
 *          Manages the arguments of the brainfuck interpreter. Stores the program,
 *          input, and output files as attributes.
 */
public class ArgumentManager {

    private String[] arguments;
    private File program = null;
    private File input = null;
    private PrintStream output = new PrintStream(System.out);
    private File tracelog = null;
    private Set<String> options;

    /**
     * Standard constructor.
     *
     * @param arguments The arguments for the interpreter.
     */
    public ArgumentManager(String[] arguments) {
        this.arguments = arguments;
        options = new HashSet<>();
    }

    /**
     * Loads and checks the interpreter arguments.
     *
     * @throws NotFoundException if one of the necessary files wasn't found
     */
    public void checkArguments() throws NotFoundException, BadArgumentsException {
        boolean foundProgram = false;
        boolean foundInput = false;
        boolean foundOutput = false;
        File outputFile = null;
        for (int i = 0; i < arguments.length; i++) {
            switch (arguments[i]) {
                case "-p":
                    if (foundProgram) {
                        throw new BadArgumentsException("only one program (-p) can be interpreted at once.");
                    }
                    if (i == arguments.length - 1) {
                        throw new BadArgumentsException("-p was used with no specified file.");
                    }
                    program = new File(arguments[i + 1]);
                    if (!program.exists()) {
                        throw new NotFoundException(arguments[i + 1]);
                    }
                    foundProgram = true;
                    i++;
                    break;
                case "-i":
                    if (foundInput) {
                        throw new BadArgumentsException("only one input file (-i) can be used at once.");
                    }
                    if (i == arguments.length - 1) {
                        throw new BadArgumentsException("-i was used with no specified file.");
                    }
                    input = new File(arguments[i + 1]);
                    if (!input.exists()) {
                        throw new NotFoundException(arguments[i + 1]);
                    }
                    foundInput = true;
                    i++;
                    break;
                case "-o":
                    if (foundOutput) {
                        throw new BadArgumentsException("only one output file (-o) can be used at once.");
                    }
                    if (i == arguments.length - 1) {
                        throw new BadArgumentsException("-o was used with no specified file.");
                    }
                    try {
                        outputFile = new File(arguments[i+1]);
                        output = new PrintStream(outputFile);
                        if (input != null && input.getAbsolutePath().equals(outputFile.getAbsolutePath())) {
                           throw new BadArgumentsException("Can't use the same file as both output and input.");
                        }
                    } catch (FileNotFoundException e) {
                        throw new NotFoundException(arguments[i + 1]);
                    }
                    foundOutput = true;
                    i++;
                    break;
                case "-m":
                    Metrics.metricsOn = true;
                    break;
                case "--rewrite":
                case "--translate":
                case "--check":
                case "--genc":
                case "--genjava":
                    options.add(arguments[i]);
                    Function.allowFunctions = false;
                    break;
                case "--trace":
                    if (program != null && program.exists()) {
                        tracelog = new File(program.getName().replace(".bf", "").replace(".bmp", "") + ".log");
                    } else
                        throw new BadArgumentsException("--trace was used before -p file.");
                    break;
                case "./bfck":
                    break;
                default:
                    throw new BadArgumentsException(arguments[i] + " is not a valid argument.");
            }
        }
        if (!foundProgram) {
            throw new BadArgumentsException("You must specify which program to process with -p [path].");
        }
        if (foundOutput && program.getAbsolutePath().equals(outputFile.getAbsolutePath())) {
            throw new BadArgumentsException("Can't use the same file as both program and output.");
        }
    }

    public File getInput() {
        return input;
    }

    public File getProgram() {
        return program;
    }

    public PrintStream getOutput() {
        return output;
    }

    public File getTracelog() {
        return tracelog;
    }

    public Set<String> getOptions() {
        return options;
    }
}
