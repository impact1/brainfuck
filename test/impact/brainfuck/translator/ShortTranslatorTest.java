package impact.brainfuck.translator;


import impact.brainfuck.operation.Operation;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by Florian on 27/10/2016.
 */
public class ShortTranslatorTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Before
    public void setUp() throws Exception {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void tearDown() throws Exception {
        System.out.close();
    }

    @Test
    public void translate() throws Exception {
        List<Operation> list = new ArrayList<>();
        list.add(Operation.INCR);
        list.add(Operation.DECR);
        list.add(Operation.JUMP);
        list.add(Operation.BACK);
        list.add(Operation.LEFT);
        list.add(Operation.RIGHT);
        list.add(Operation.IN);
        list.add(Operation.OUT);
        ShortTranslator shortTranslator = new ShortTranslator();
        shortTranslator.translate(list, System.out);
        assertTrue("+-[]<>,.\n".equals(outContent.toString()) || "+-[]<>,.\r\n".equals(outContent.toString()));
    }

}