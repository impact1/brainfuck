package impact.brainfuck.reader;

import impact.brainfuck.exception.BadArgumentsException;
import impact.brainfuck.exception.NotFoundException;
import impact.brainfuck.exception.ReadException;
import impact.brainfuck.exception.SyntaxErrorException;
import impact.brainfuck.macro.FunctionCall;
import impact.brainfuck.operation.Operation;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.rules.TemporaryFolder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static impact.brainfuck.operation.Operation.*;
import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Florian Bourniquel, Guillaume André, Jérémy Lara
 * @version 1.0
 *          Test for TextReader
 */
public class TextReaderTest {

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    @Test //(expected = NotFoundException.class)
    public void FileNotFoundException() throws Exception {
        File file = new File("/dfndkfndknkqnqkn.txt"); //impossible file name
        exit.expectSystemExitWithStatus(3);
        new TextReader(file);
    }

    @Test
    public void readTest() throws IOException, NotFoundException, SyntaxErrorException, ReadException, BadArgumentsException {
        File file = doTestFile("+ -  \t >< ., ][\r" + "#ignore this \n" + "+++\n");
        TextReader reader = new TextReader(file);
        List<Operation> got = reader.read();
        List<Operation> expected = new ArrayList<>();
        expected.add(INCR);
        expected.add(DECR);
        expected.add(RIGHT);
        expected.add(LEFT);
        expected.add(OUT);
        expected.add(IN);
        expected.add(BACK);
        expected.add(JUMP);
        for (int i = 0; i < 3; i++) {
            expected.add(INCR);
        }
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), got.get(i));
        }
    }

    private File doTestFile(String content) throws IOException {
        File file = folder.newFile();
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(content);
        bw.close();
        return file;
    }

    @Test
    public void longSyntax() throws NotFoundException, ReadException, SyntaxErrorException, IOException, BadArgumentsException {
        File file = doTestFile("INCR\nOUT");
        TextReader reader = new TextReader(file);
        List<Operation> got = reader.read();
        List<Operation> expected = new ArrayList<>();
        expected.add(INCR);
        expected.add(OUT);
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), got.get(i));
        }
    }

    @Test // expected = SyntaxErrorException.class
    public void invalidLong() throws IOException, NotFoundException, SyntaxErrorException, ReadException, BadArgumentsException {
        File file = doTestFile("NOPE");
        TextReader reader = new TextReader(file);
        exit.expectSystemExitWithStatus(5);
        reader.read();
    }

    @Test //expected = SyntaxErrorException.class
    public void invalidSymbols() throws IOException, NotFoundException, SyntaxErrorException, ReadException, BadArgumentsException {
        File file = doTestFile("^é'éé***'");
        TextReader reader = new TextReader(file);
        exit.expectSystemExitWithStatus(5);
        reader.read();
    }

    @Test
    public void macroTest() throws IOException, NotFoundException, SyntaxErrorException, ReadException, BadArgumentsException {
        File file = doTestFile("lazy5?+++/\n" + "iori2?---/\r" + "lazy5+iori2> lazy5");
        TextReader reader = new TextReader(file);
        List<Operation> got = reader.read();
        List<Operation> expected = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            expected.add(INCR);
        }
        expected.add(INCR);
        for (int i = 0; i < 3; i++) {
            expected.add(DECR);
        }
        expected.add(RIGHT);
        for (int i = 0; i < 3; i++) {
            expected.add(INCR);
        }
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), got.get(i));
        }
    }

    @Test
    public void nestedMacro() throws IOException, NotFoundException, SyntaxErrorException, ReadException, BadArgumentsException {
        File file = doTestFile("lazy5?+++/\n" + "iori2?--lazy5+/\r" + "iori2");
        TextReader reader = new TextReader(file);
        List<Operation> got = reader.read();
        List<Operation> expected = new ArrayList<>();
        expected.add(DECR);
        expected.add(DECR);
        for (int i = 0; i < 3; i++) {
            expected.add(INCR);
        }
        expected.add(INCR);
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), got.get(i));
        }
    }

    @Test
    public void invalidMacro() throws IOException, NotFoundException, SyntaxErrorException, ReadException, BadArgumentsException {
        File file = doTestFile("nope2");
        TextReader reader = new TextReader(file);
        exit.expectSystemExitWithStatus(5);
        reader.read();
    }

    @Test
    public void invalidMacroDefinition() throws IOException, NotFoundException, SyntaxErrorException, ReadException, BadArgumentsException {
        File file = doTestFile("testfail? --ghj++/");
        TextReader reader = new TextReader(file);
        exit.expectSystemExitWithStatus(5);
        reader.read();
    }

    @Test
    public void forgotMacroEnd() throws NotFoundException, ReadException, SyntaxErrorException, IOException, BadArgumentsException {
        File file = doTestFile("testfail? --+++++ \n testfail ++");
        TextReader reader = new TextReader(file);
        exit.expectSystemExitWithStatus(5);
        reader.read();
    }

    @Test
    public void multiLineMacro() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("a? -- \n\t OUT \nIN/ \n a");
        TextReader reader = new TextReader(file);
        List<Operation> got = reader.read();
        List<Operation> expected = new ArrayList<>();
        expected.add(DECR);
        expected.add(DECR);
        expected.add(OUT);
        expected.add(IN);
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), got.get(i));
        }
    }

    @Test
    public void instructionsAfterMacroEnd() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("dont? ++++ \t\n --- / +++ \n dont");
        TextReader reader = new TextReader(file);
        exit.expectSystemExitWithStatus(5);
        reader.read();
    }

    @Test
    public void macroParam() throws IOException, NotFoundException, SyntaxErrorException, ReadException, BadArgumentsException {
        File file = doTestFile("droite? ...+/ \r mul?-/\n [droite*11mul*2\n++");
        TextReader reader = new TextReader(file);
        List<Operation> got = reader.read();
        List<Operation> expected = new ArrayList<>();
        expected.add(JUMP);
        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 3; j++) {
                expected.add(OUT);
            }
            expected.add(INCR);
        }
        expected.add(DECR);
        expected.add(DECR);
        expected.add(INCR);
        expected.add(INCR);
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i).name(), got.get(i).name());
        }
    }

    @Test
    public void wrongParam() throws IOException, NotFoundException, SyntaxErrorException, ReadException, BadArgumentsException {
        File file = doTestFile("jk? +++ /\n jk*mosser");
        TextReader reader = new TextReader(file);
        exit.expectSystemExitWithStatus(5);
        reader.read();
    }

    @Test
    public void hasSetParamSub() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("a? +++/\n b? a*/\n c? b(1) -- /\n c");
        TextReader reader = new TextReader(file);
        List<Operation> got = reader.read();
        List<Operation> expected = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            expected.add(INCR);
        }
        expected.add(DECR);
        expected.add(DECR);
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), got.get(i));
        }
    }

    //supports up to macro*1000000
    @Test
    public void longEndOfFileMacro() throws NotFoundException, SyntaxErrorException, IOException, ReadException, BadArgumentsException {
        File file = doTestFile("bug? .............../ \n bug*100000");
        TextReader reader = new TextReader(file);
        List<Operation> got = reader.read();
        List<Operation> expected = new ArrayList<>();
        for (int i = 0; i < 15 * 100000; i++) {
            expected.add(OUT);
        }
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), got.get(i));
        }
    }

    @Test
    public void extractParams() throws IOException, NotFoundException, SyntaxErrorException {
        TextReader tx = new TextReader(folder.newFile());
        List<Integer> got = tx.parseParams("(8&&3)");
        assertEquals(new Integer(8), got.get(0));
        assertEquals(new Integer(3), got.get(1));
    }

    @Test
    public void illegalPass() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("a? +++/\n b? a* /\n c? ++b /\n ++c");
        TextReader tx = new TextReader(file);
        exit.expectSystemExitWithStatus(5);
        tx.read();
    }

    @Test
    public void multipleSetParams() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("a? +++/\n c? a* ---/\n d? c(2)/\n e? c(3)/\n d e");
        TextReader tx = new TextReader(file);
        List<Operation> got = tx.read();
        List<Operation> expected = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            expected.add(INCR);
        }
        for (int i = 0; i < 3; i++) {
            expected.add(DECR);
        }
        for (int i = 0; i < 9; i++) {
            expected.add(INCR);
        }
        for (int i = 0; i < 3; i++) {
            expected.add(DECR);
        }
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), got.get(i));
        }
    }

    @Test
    public void macroFinals() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("a? ---/\n b? +++/\n c? a* b*/ \n d? c(%&%)*2 a*/\n ...d(1&2&3)a++");
        TextReader reader = new TextReader(file);
        List<Operation> got = reader.read();
        List<Operation> expected = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            expected.add(OUT);
        }
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 3; j++) {
                expected.add(DECR);
            }
            for (int j = 0; j < 6; j++) {
                expected.add(INCR);
            }
        }
        for (int i = 0; i < 12; i++) {
            expected.add(DECR);
        }
        expected.add(INCR);
        expected.add(INCR);
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), got.get(i));
        }
    }

    //********************
    //FUNCTION BASED TESTS
    //********************

    @Test
    public void basicFunction() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("test! +++/\n test --");
        TextReader reader = new TextReader(file);
        List<Operation> got = reader.read();
        List<Operation> expected = new ArrayList<>();
        expected.add(DECR);
        expected.add(DECR);
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), got.get(i));
        }
        Map<Integer, List<FunctionCall>> functions = reader.getFunctions();
        expected = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            expected.add(INCR);
        }
        got = functions.get(0).get(0).loadList();
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), got.get(i));
        }
    }

    @Test
    public void functionLocation() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("test! +++/\n +++ test --");
        TextReader reader = new TextReader(file);
        reader.read();
        Map<Integer, List<FunctionCall>> functions = reader.getFunctions();
        assertEquals(1, functions.keySet().size());
        assert functions.get(3) != null;
    }

    @Test
    public void locationInMacro() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("test! +++/\n a? ..test++/\n ,,a++a");
        TextReader reader = new TextReader(file);
        reader.read();
        Map<Integer, List<FunctionCall>> functions = reader.getFunctions();
        assertEquals(2, functions.keySet().size());
        assert functions.get(4) != null;
        assert functions.get(10) != null;
    }

    @Test
    public void simpleInput() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("test!\" +++/ \n ---test\"ntr\"--");
        TextReader reader = new TextReader(file);
        reader.read();
        Map<Integer, List<FunctionCall>> functions = reader.getFunctions();
        assertEquals("ntr", functions.get(3).get(0).getInput());
    }

    @Test
    public void locationRepeatMacro() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("test! +++/\n a? --- test ... /\n a*3");
        TextReader reader = new TextReader(file);
        reader.read();
        Map<Integer, List<FunctionCall>> functions = reader.getFunctions();
        assert functions.get(3) != null;
        assert functions.get(9) != null;
        assert functions.get(15) != null;
    }

    @Test
    public void locationRepeatInMacro() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("test!\"$ +++/\n" +
                " a?  --- test\"%\" /\n" +
                " b? a*\"%\"/\n" +
                " b(3)\"er\"");
        TextReader reader = new TextReader(file);
        reader.read();
        Map<Integer, List<FunctionCall>> functions = reader.getFunctions();
        assert functions.get(3) != null;
        assert functions.get(6) != null;
        assert functions.get(9) != null;
    }

    @Test
    public void functionWithinMacroInput() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("test!\" +++/\n a?  --- test\"%\" /\n a\"rom\"");
        TextReader reader = new TextReader(file);
        reader.read();
        Map<Integer, List<FunctionCall>> functions = reader.getFunctions();
        assertEquals("rom", functions.get(3).get(0).getInput());
    }

    @Test
    public void functionInFunction() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("a!\" +++/\n b!\" a\"%\" /\n a\"rom\" b\"input for b&input for a\"");
        TextReader reader = new TextReader(file);
        reader.read();
        Map<Integer, List<FunctionCall>> functions = reader.getFunctions();
        assertEquals("rom", functions.get(0).get(0).getInput());
        assertEquals("input for b", functions.get(0).get(1).getInput());
        assertEquals("input for a", functions.get(0).get(1).getAllInputs().get(0));
    }

    @Test
    public void fixedInputFunction() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("a!\" +++/\n " +
                " b!\" a\"test\"/\n" +
                " b\"input for b\"");
        TextReader reader = new TextReader(file);
        reader.read();
        Map<Integer, List<FunctionCall>> functions = reader.getFunctions();
        assertEquals("input for b", functions.get(0).get(0).getInput());
        List<Operation> got = functions.get(0).get(0).loadList();
        assert got.isEmpty();
        assertEquals("a", functions.get(0).get(0).getNestedFunctions().get(0).get(0).getFunction().getName());
    }

    @Test
    public void functionAndMacro() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("a!? +++");
        TextReader reader = new TextReader(file);
        exit.expectSystemExitWithStatus(5);
        reader.read();
    }

    @Test
    public void complexFunction() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("a? +++/ \n b? a*/ \n c? b(%) ... / \n test!\" c(3) b(%) a* / \n a b(2) test(1&2)\"snow\" ,,,");
        TextReader reader = new TextReader(file);
        List<Operation> got = reader.read();
        List<Operation> expected = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            expected.add(INCR);
        }
        for (int i = 0; i < 3; i++) {
            expected.add(IN);
        }
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), got.get(i));
        }
        Map<Integer, List<FunctionCall>> functions = reader.getFunctions();
        assertEquals("snow", functions.get(9).get(0).getInput());
        expected = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            expected.add(INCR);
        }
        for (int i = 0; i < 3; i++) {
            expected.add(OUT);
        }
        for (int i = 0; i < 9; i++) {
            expected.add(INCR);
        }
        got = functions.get(9).get(0).loadList();
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), got.get(i));
        }
    }

    @Test
    public void functionWithReturnCheck() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("test!$ +++/ \n test");
        TextReader reader = new TextReader(file);
        reader.read();
        List<Operation> expected = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            expected.add(INCR);
        }
        Map<Integer, List<FunctionCall>> functions = reader.getFunctions();
        assertTrue(functions.get(0).get(0).getFunction().isAllowReturn());
        assertTrue(functions.get(0).get(0).hasOutput());
        assertEquals(expected, functions.get(0).get(0).loadList());
    }

    @Test
    public void recursiveFunction() throws IOException, NotFoundException, ReadException, SyntaxErrorException, BadArgumentsException {
        File file = doTestFile("test!$ ++ test ++ / \n test");
        TextReader reader = new TextReader(file);
        reader.read();

        Map<Integer, List<FunctionCall>> functions = reader.getFunctions();
        assertEquals("test", functions.get(0).get(0).getFunction().getName());
        functions.get(0).get(0).loadList();
        assertEquals("test",functions.get(0).get(0).getNestedFunctions().get(2).get(0).getFunction().getName());
    }

}