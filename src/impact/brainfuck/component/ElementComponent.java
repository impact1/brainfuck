package impact.brainfuck.component;

import impact.brainfuck.exception.SyntaxErrorException;
import impact.brainfuck.macro.DefinedElements;
import impact.brainfuck.macro.FunctionCall;
import impact.brainfuck.macro.Macro;
import impact.brainfuck.operation.Operation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Guillaume André
 *         <p>
 *         Component containing a macro or a function.
 *         For example, in the macro a? test* b(%) c"input"/
 *         the components are test*, b(%) and c"input".
 *         Stores parameters and inputs of the element.
 *         Avoid storing the same element multiple times with different parameters.
 */
public class ElementComponent implements Component {

    boolean repeatFirst = true;
    private Macro macro;
    private int repeats;
    private List<Integer> passedParams;
    private List<Integer> parameters = new ArrayList<>();
    private int paramRequired;
    private int inputsRequired;
    private List<String> inputPass;
    private List<String> inputList;
    private DefinedElements definedElements;

    /**
     * Builds an element component from its corresponding expression.
     *
     * @param expression      - the String corresponding to this component
     * @param definedElements - the list of already defined elements
     * @throws SyntaxErrorException - in case of a wrong number of parameters, or if extra tokens are present
     */
    public ElementComponent(String expression, DefinedElements definedElements) throws SyntaxErrorException {
        this.definedElements = definedElements;
        this.passedParams = new ArrayList<>();
        this.inputPass = new ArrayList<>();
        inputsRequired = 0;
        expression = extractMacroFromComponent(expression);
        expression = treatInput(expression);
        repeats = 1;
        paramRequired = 0;
        Pattern first = Pattern.compile("\\*|[%0-9 &()]+");
        Matcher matcher = first.matcher(expression);
        for (int pass = 0; pass < 2; pass++) {
            if (matcher.find()) {
                if (matcher.group().contains("*")) {
                    expression = extractRepeats(expression);
                } else {
                    expression = extractParameters(expression, pass);
                }
            }
        }
        Macro.checkForExtraCharacters(expression);
    }

    /**
     * Extracts the name of the macro/function of this component.
     * Searches for the corresponding element in the list of defined elements.
     * Sets up the found element as this component's function/macro.
     *
     * @param expression - the String corresponding to this component
     * @return the same String, without the name of the function/macro
     * @throws SyntaxErrorException - if the element used was not defined beforehand
     */
    public String extractMacroFromComponent(String expression) throws SyntaxErrorException {
        Pattern detectName = Pattern.compile("[0-9a-z_]+");
        Matcher matcher = detectName.matcher(expression);
        if (!matcher.find()) {
            throw new SyntaxErrorException("Illegal operators in element definition: \"" + expression + "\"");
        }
        macro = definedElements.getMacro(matcher.group());
        if (macro == null) {
            throw new SyntaxErrorException("Element \"" + matcher.group() + "\" was not found.");
        }
        return expression.replace(matcher.group(), "");
    }

    /**
     * Analyzes the given expression and extracts the inputs that this element must use.
     *
     * @param expression - the String corresponding to this component
     * @return the same String,without the inputs tokens
     * @throws SyntaxErrorException - if an incorrect number of inputs was used
     */
    public String treatInput(String expression) throws SyntaxErrorException {
        inputPass = new ArrayList<>();
        Pattern findInput = Pattern.compile("\"[^\"]+\"");
        Matcher inputMatcher = findInput.matcher(expression);
        if (inputMatcher.find()) {
            inputPass.addAll(analyzeInputs(inputMatcher.group()));
            expression = inputMatcher.replaceFirst("");
        }
        if (inputPass.size() != macro.getInputCount()) {
            throw new SyntaxErrorException("Incorrect input count for element \"" + macro.getName() + "\": it was called with " + inputPass.size() +
                    " instead of " + macro.getInputCount() + ".");
        }
        return expression;
    }

    /**
     * Translates the input String of an element into its corresponding list of inputs.
     * For example, "input1&%&input2" will be converted into a list containing:
     * input1, null (because determined when the element is called) and input2.
     * Does not check for syntax errors.
     *
     * @param expression - the String of the inputs ("input1&input2")
     * @return the corresponding list of inputs
     */
    public List<String> analyzeInputs(String expression) {
        Pattern separateInputs = Pattern.compile("[^&\"]+");
        Matcher matcher = separateInputs.matcher(expression);
        List<String> result = new ArrayList<>();
        boolean replacedDefault = false;
        while (matcher.find()) {
            if (matcher.group().equals("%")) {
                result.add(null);
                inputsRequired++;
            } else {
                result.add(matcher.group());
            }
        }
        return result;
    }

    /**
     * Extracts the number of times the element must be repeated when the macro/function is called.
     * If this is determined by one of the parameters, the number of repeats is set as -1.
     * If the token for repetition (*) is absent, the element will be inserted only one time.
     *
     * @param expression - the String containing the parameters
     * @return the same String, without the tokens and parameters tied to macro repetition
     */
    public String extractRepeats(String expression) {
        Pattern getRepeats = Pattern.compile("\\*");
        Matcher matcherRepeat = getRepeats.matcher(expression);
        if (matcherRepeat.find()) {
            Pattern getRepeatsCount = Pattern.compile("\\*[0-9]+");
            Matcher matcherRepeatCount = getRepeatsCount.matcher(expression);
            if (matcherRepeatCount.find()) {
                repeats = Integer.parseInt(matcherRepeatCount.group().substring(1));
                expression = matcherRepeatCount.replaceFirst("");
            } else {
                paramRequired++;
                repeats = -1;
            }
        }
        return expression.replaceFirst("\\*", "");
    }

    /**
     * Extracts the parameters for this component's element.
     * Sets whether the parameters comes before the number of times the element must be repeated, or vice-versa.
     *
     * @param expression - the String containing the parameters of this element
     * @param pass       - 0 if this was found first, 1 otherwise
     * @return the same String, without the parameters of the element
     * @throws SyntaxErrorException - if a wrong number of parameters was used
     */
    public String extractParameters(String expression, int pass) throws SyntaxErrorException {
        Pattern getPasses = Pattern.compile("\\([%0-9 &]+\\)");
        Matcher matcherPasses = getPasses.matcher(expression);
        if (matcherPasses.find()) {
            passedParams = analyzeConstantParams(matcherPasses.group());
            if (passedParams.size() != macro.getParamCount()) {
                throw new SyntaxErrorException("The element " + macro.getName() + " was called with " + passedParams.size() +
                        " parameters instead of " + macro.getParamCount() + ".");
            }
            for (Integer param : passedParams) {
                if (param == -1) {
                    paramRequired++;
                }
            }
            if (pass == 1) {
                repeatFirst = false;
            }
            expression = matcherPasses.replaceFirst("");
        }
        return expression;
    }

    /**
     * Translates a String containing the parameters of a macro in a macro definition.
     * For example, this may convert (1&%&3) into [1,-1,3]
     * Parameters which are set when invoking the macro are stored as -1 in the parameter list.
     * Does not check for syntax errors - this must be done elsewhere.
     *
     * @param expression - the String to translate
     * @return a list containing all of the parameters in order of appearance
     */
    public List<Integer> analyzeConstantParams(String expression) {
        Pattern paramsAndSeparator = Pattern.compile("%|[0-9]+");
        Matcher matcher = paramsAndSeparator.matcher(expression);
        List<Integer> result = new ArrayList<>();
        while (matcher.find()) {
            if (matcher.group().equals("%")) {
                result.add(-1);
            } else {
                result.add(Integer.parseInt(matcher.group()));
            }
        }
        return result;
    }

    /**
     * Gives the element contained in this component.
     */
    public Macro getMacro() {
        return macro;
    }

    /**
     * Gives the number of times the element of this component should be repeated.
     * Returns -1 if determined in brainfuck code.
     */
    public int getRepeats() {
        return repeats;
    }

    /**
     * Gives the number of parameters required for the element contained within this component.
     * This may be different than the actual parameter count of the contained elements.
     * For example, a component containing the element "a" contained within the element "test" :
     * test? a(5&%)
     * "a" has a parameter count of 2, but the first parameter is always 5.
     * The number of required parameters in this case would only be 1.
     */
    public int getParamRequired() {
        return paramRequired;
    }

    /**
     * Gives the number of inputs required for the element contained within this component.
     * This may be different than the actual input count of the contained elements.
     * For example, a component containing the element "a" contained within the element "test" :
     * test? a"static&%"
     * "a" has an input count of 2, but the first parameter is always "static".
     * The number of inputs in this case would only be 1.
     */
    public int getInputsRequired() {
        return inputsRequired;
    }

    /**
     * Gives a the list containing all of the static parameters before invocation.
     * Non static parameters (determined in the actual brainfuck code) are stored as -1.
     */
    public List<Integer> getPassedParams() {
        return passedParams;
    }

    /**
     * Gives a the list containing all of the static inputs before invocation.
     * Non static inputs (determined in the actual brainfuck code) are stored as null.
     */
    public List<String> getInputPass() {
        return inputPass;
    }

    public void setInputPass(List<String> inputPass) {
        this.inputPass = inputPass;
    }

    /**
     * Sets whether the repeat amount should be queried first.
     * Allows separation of cases a*(%) and a(%)*
     * Default is true.
     *
     * @param repeatFirst true if the repeat amount comes before the parameters, false otherwise
     */
    public void setRepeatFirst(boolean repeatFirst) {
        this.repeatFirst = repeatFirst;
    }

    /**
     * Translates this component into its corresponding brainfuck operations.
     * Extracts the required parameters and inputs from the given lists.
     * Stores the updated lists to give them back later.
     *
     * @param params           - the parameters acquired in brainfuck code
     * @param instructionsSize - the number of instructions read before - used to properly position function calls
     * @param functions        - the map where functions calls should be inserted
     * @param inputs           - the inputs acquired in brainfuck code
     * @return the list of instructions of this component
     * @throws SyntaxErrorException - if not enough parameters were used
     */
    public List<Operation> componentToList(List<Integer> params, int instructionsSize, Map<Integer, List<FunctionCall>> functions, List<String> inputs) throws SyntaxErrorException {
        int storeRepeat = repeats;
        List<Integer> storePassed = passedParams;
        List<String> storeInput = inputPass;
        if (params.size() < paramRequired) {
            throw new SyntaxErrorException("Not enough parameters for element \"" + macro.toString() + "\".");
        }
        if (inputs.size() < inputsRequired) {
            throw new SyntaxErrorException("Not enough inputs for element \"" + macro.toString() + "\".");
        }
        parameters = extractParams(params);
        inputList = extractInputs(inputs);
        List<Operation> toRepeat = macro.toList(passedParams, inputPass, instructionsSize, functions, false);
        Map<Integer, List<FunctionCall>> repeatInput = new HashMap<>();
        //repeat functions call when repeating a function
        for (int call : functions.keySet()) {
            if (call > instructionsSize) {
                repeatInput.put(call - instructionsSize, functions.get(call));
            }
        }
        List<Operation> result = new ArrayList<>();
        for (int i = 0; i < repeats; i++) {
            if (i > 0 && !repeatInput.isEmpty()) {
                for (int call : repeatInput.keySet()) {
                    functions.put(instructionsSize + result.size() + call, repeatInput.get(call));
                }
            }
            result.addAll(toRepeat);
        }
        repeats = storeRepeat;
        passedParams = storePassed;
        inputPass = storeInput;
        return result;
    }

    /**
     * Gives the updated parameters list after componentToList() was used.
     *
     * @return the parameters list after the required parameters were extracted
     */
    @Override
    public List<Integer> getParametersAfterToList() {
        return parameters;
    }

    /**
     * Gives the updated input list after componentToList() was used.
     *
     * @return the input list after the required parameters were extracted
     */
    @Override
    public List<String> getInputsAfterToList() {
        return inputList;
    }

    /**
     * Extracts required parameters for component construction from the  parameters found in brainfuck code.
     * Removes them from said list before returning it.
     *
     * @param original - the parameters list : [1,2,3] for (1&2&3)
     * @return the same list, without its extracted elements
     */
    public List<Integer> extractParams(List<Integer> original) {
        if (repeats == -1) {
            if (repeatFirst) {
                repeats = original.remove(0);
            } else {
                repeats = original.remove(passedParams.size());
            }
        }
        List<Integer> finalParams = new ArrayList<>();
        for (int p : passedParams) {
            if (p == -1) {
                finalParams.add(original.remove(0));
            } else {
                finalParams.add(p);
            }
        }
        this.passedParams = finalParams;
        return original;
    }

    /**
     * Extracts required inputs for component construction from the inputs found in brainfuck code.
     * Removes them from said list before returning it.
     *
     * @param givenInputs - the inputs list : [input1,input2] for "input1&input2"
     * @return the same list, without the extracted elements
     */
    public List<String> extractInputs(List<String> givenInputs) {
        List<String> finalInputs = new ArrayList<>();
        for (String input : inputPass) {
            if (input == null) {
                finalInputs.add(givenInputs.remove(0));
            } else {
                finalInputs.add(input);
            }
        }
        this.inputPass = finalInputs;
        return givenInputs;
    }

    @Override
    public String toString() {
        return macro.getName();
    }
}
