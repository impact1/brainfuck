package impact.brainfuck.exception;

/**
 * Thrown by the LoopChecker whenever one of the JUMP or BACK instructions was
 * not properly linked.
 * 
 * @author Guillaume Andre
 * @version 2.0
 */
public class LoopException extends Exception {

	private static final long serialVersionUID = 5610419662150392315L;

	/**
	 * @param instruction - "JUMP" or "BACK" depending on instruction that was
	 * not properly linked.
	 */
	public LoopException(String instruction) {
		super("One of the " + instruction + " instruction in the program is not properly associated.");
		System.out.println(this.getMessage());
		System.exit(4);
	}
}
