package impact.brainfuck.exception;

/**
 * Thrown when non-bf and non-bmp file was included in the parameters, when
 * several -p, -i or -o statements are used, when -p, -i or -o is used without a
 * file name, --trace was used before -p statement (thus program name cannot be
 * retrieved to write program.log), an invalid argument is used, or the -p
 * argument is not used.
 * 
 * @author Guillaume Andre
 * @version 1.0
 */
public class BadArgumentsException extends Exception {

	private static final long serialVersionUID = 7168898750326576962L;

	public BadArgumentsException() {
		this("");
	}

	/**
	 * @param cause - the cause of the argument issue (see above)
	 */
	public BadArgumentsException(String cause) {
		super("Incorrect arguments; " + cause);
		System.out.println(this.getMessage());
		System.exit(6);
	}
}
