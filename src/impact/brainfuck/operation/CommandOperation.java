package impact.brainfuck.operation;

import impact.brainfuck.exception.*;
import impact.brainfuck.memory.Memory;

/**
 * @author Florian Bourniquel, Guillaume Andre
 * @version 2.0
 *          <p>
 *          Interface of the strategy operation
 */
public interface CommandOperation {

    /**
     * Does the requested operation.
     * The return value is the same as currentCellInstruction, except for JUMP and BACK.
     *
     * @param memory                  - the memory in its current state
     * @param currentInstructionIndex - the instruction pointer
     * @return where the instruction pointer should be after interpretation of the command
     * @throws CellOverflowException - if a cell overflows
     * @throws PointerException      - if the memory pointer goes out of bounds
     */
    int doOperation(Memory memory, int currentInstructionIndex) throws CellOverflowException, PointerException,
            OutputException, LoopException, InputException, AsciiException;
}
