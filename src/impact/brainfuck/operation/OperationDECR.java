package impact.brainfuck.operation;

import impact.brainfuck.exception.CellOverflowException;
import impact.brainfuck.memory.Memory;

/**
 * @author Florian Bourniquel, Guillaume Andre
 * @version 2.0
 *          <p>
 *          Implementation of the Interface CommandOperation.
 *          This class decrements the pointed memory cell by one.
 */
public class OperationDECR implements CommandOperation {

    /**
     * Decrements the current memory cell by one.
     *
     * @param memory                  - memory where the value is decremented
     * @param currentInstructionIndex - instruction index
     * @throws CellOverflowException - if value in memory at pointer is 0
     */
    @Override
    public int doOperation(Memory memory, int currentInstructionIndex) throws CellOverflowException {
        if (memory.getMemory()[memory.getCurrentCellIndex()] == -128) {
            throw new CellOverflowException(memory.getCurrentCellIndex(), 0, currentInstructionIndex);
        }
        memory.getMemory()[memory.getCurrentCellIndex()]--;
        return currentInstructionIndex + 1;
    }
}



