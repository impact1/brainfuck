package impact.brainfuck.operation;

import impact.brainfuck.exception.CellOverflowException;
import impact.brainfuck.exception.PointerException;
import impact.brainfuck.memory.Memory;

/**
 * @author Florian Bourniquel, Guillaume Andre
 * @version 2.0
 *          <p>
 *          Implementation of the Interface CommandOperation.
 *          This class moves the memory pointer to the left.
 */
public class OperationLEFT implements CommandOperation {

    /**
     * Moves the memory pointer 1 cell to the left.
     *
     * @param memory                  - the current memory of the brainfuck program
     * @param currentInstructionIndex - the instruction pointer
     * @throws PointerException - if the pointer tries to go below 0
     */
    @Override
    public int doOperation(Memory memory, int currentInstructionIndex) throws PointerException {
        if (memory.getCurrentCellIndex() == CellOverflowException.minLimit) {
            throw new PointerException(memory.getCurrentCellIndex(), currentInstructionIndex);
        }
        memory.setMemoryPointer((short) (memory.getCurrentCellIndex() - 1));
        return currentInstructionIndex + 1;
    }
}
