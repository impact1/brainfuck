package impact.brainfuck.operation;

import impact.brainfuck.exception.OutputException;
import impact.brainfuck.memory.Memory;

import java.io.PrintStream;

/**
 * @author Guillaume Andre
 * @version 2.0
 * 
 * Implementation of interface commendOperation. Prints or writes the character
 * associated to the current memory cell as output.
 */
public class OperationOUT implements CommandOperation {

	private static PrintStream outStream;

	public static void setOutStream(PrintStream ps) {
		OperationOUT.outStream = ps;
	}

	/**
	 * Outputs current memory cell value as an ASCII character.
	 *
	 * @param memory - the memory in its current state
	 * @param currentInstructionIndex - the instruction pointer
	 * @return the instruction pointer moved to the next instruction
	 * @throws OutputException - if it fails to write the output file
	 */
	@Override
	public int doOperation(Memory memory, int currentInstructionIndex) throws OutputException {
		outStream.print((char) (memory.getMemory()[memory.getCurrentCellIndex()] + 128));
		return currentInstructionIndex + 1;
	}
}
