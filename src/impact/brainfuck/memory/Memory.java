package impact.brainfuck.memory;

import impact.brainfuck.exception.OutOfMemory;

/**
 * @author Elliot Kauffmann, Guillaume Andre
 * @version 1.0
 * 
 * Computational Model
 * Memory class
 */
public class Memory {

    private byte[] memory;
    private short currentCellIndex;
    
    /**
     * Creates a new Memory instance, with memoryPointer pointing at cell 0.
     */
    public Memory() {
        memory = new byte[30000];
        for (int i = 0; i<memory.length; i++) {
            memory[i] = -128;
        }
    	currentCellIndex = 0;
    }
    
    
    /**
     * Memory 30,000 cells matrix accessor
     * @return byte[] memory
     */
    public byte[] getMemory() {
    	return memory;
    }
    /**
     * Index of the current cell in the memory : accessor
     * @return byte currentCellIndex
     */
    public short getCurrentCellIndex() {
    	return currentCellIndex;
    }
    
    /**
     * Index of the current cell in the memory : setter
     * @param currentCellIndex the current cell index
     */
    public void setMemoryPointer(short currentCellIndex) {
    	this.currentCellIndex = currentCellIndex;
    }

    /**
     * Display the relevant cells of the memory
     */
    public String display() {
        String s = "";
        for (int i = 0; i < memory.length ; i++) {
            if (memory[i] != -128)
                s = s + "C" + i + ": " + (memory[i] + 128) + "\n";
        }
        return s;
    }

    /**
     * find the last memory cell at 0 starting from the end of memory[]
     * @return int result
     */
    public int findLastUnusedCell() throws OutOfMemory {
        int result = 29999;
        for (; result > 1 && memory[result] == -128 ; result--) {

        }
        if (result == 29999)
            throw new OutOfMemory();
        return ++result;
    }

    /**
     * set the memory cell [index] with the current memory cell
     * @param index the index of the cell
     */
    public void setMemoryCellWithCurrentCell(short index) {
        memory[index] = memory[getCurrentCellIndex()];
    }

    /**
     * set the current memory cell with x
     * @param x value
     */
    public void setCurrentMemoryCell(byte x) {
        memory[getCurrentCellIndex()] = x;
    }

    /**
     * return the value stored in the current memory cell
     * @return byte value
     */
    public byte getCurrentMemoryCell() {
       return memory[getCurrentCellIndex()];
    }

    /**
     * set the memory cell min to 29 999 at 0
     * @param min first cell
     */
    public void cleanFunctionMemory(int min){
        for (int i = min; i < 30000 ; i++) {
            memory[i] = -128;
        }
    }
}
