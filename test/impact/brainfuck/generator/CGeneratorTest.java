package impact.brainfuck.generator;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import impact.brainfuck.operation.Operation;

/**
 * @author Elliot Kauffmann
 * 
 * This class is the JUnit test class for the C code generator.
 * 
 * It tests whether the beginning and end of the generated code contains
 * specific Strings.
 * 
 * It also tests whether the code generator is capable of generating individual
 * instructions, and finally it tests whether it is capable of translating all
 * the instructions in the correct order.
 */
public class CGeneratorTest {

	/** Test file that holds the code to be tested */
	File cGenTestFile;
	/** Code Generator */
	CGenerator cGen;
	/** Instructions to be translated */
	List<Operation> list;

	/** Test file reader */
	FileReader fr;
	/** Reads the lines of the test file one after the other */
	BufferedReader br;

	/**
	 * Initializes a temporary file for the tests to write in, the instruction
	 * List and the code generator.
	 * 
	 * Also initializes a FileReader and a BufferedReader to read the file after
	 * we've written in it.
	 */
	@Before
	public void setUp() throws Exception {

		cGenTestFile = new File("cGen.txt");
		cGenTestFile.deleteOnExit();
		System.setOut(new PrintStream(cGenTestFile));
		cGen = new CGenerator();
		list = new ArrayList<>();

		fr = new FileReader(cGenTestFile);
		br = new BufferedReader(fr);
	}

	/**
	 * Clean everything up after the test.
	 */
	@After
	public void tearDown() throws Exception {

		fr.close();
		br.close();

		cGenTestFile.delete();
		System.out.close();
	}

	/**
	 * This tests whether the beginning and end of the generated code contains
	 * specific lines.
	 */
	@Test
	public void beginningAndEndTest() throws Exception {

		cGen.generate(list, System.out);
		String expectedBeginningLines[] = { "#include <stdio.h>\n", "#include <stdlib.h>\n", "typedef unsigned char Byte;\n", "int main() {\n",
				"Byte* p = malloc(30000 * sizeof(Byte));\n" };
		String expectedEndLines[] = { "return 0;", "}" };
		StringBuilder allLines = new StringBuilder();
		String line;

		// Read all the lines up to the code generation marker
		do {
			line = new String(br.readLine());
			allLines.append(line).append("\n");
		} while (!(line.contains("// Code generation from BrainF*ck to C begins here")));

		line = allLines.toString();
		allLines = new StringBuilder();
		// Not using a for Loop, so we know which line is not being represented
		// in case of an error.
		assertTrue(line.contains(expectedBeginningLines[0]));
		assertTrue(line.contains(expectedBeginningLines[1]));
		assertTrue(line.contains(expectedBeginningLines[2]));
		assertTrue(line.contains(expectedBeginningLines[3]));
		assertTrue(line.contains(expectedBeginningLines[4]));

		// Read all the lines to the end of the file.
		do {
			line = br.readLine();
			allLines.append(line).append("\n");
		} while (line != null);

		line = allLines.toString();

		assertTrue(line.contains(expectedEndLines[0]));
		assertTrue(line.contains(expectedEndLines[1]));
	}

	/**
	 * The next 8 methods test whether the code generator is capable of
	 * generating individual instructions.
	 */

	@Test
	public void incrTest() throws Exception {

		list.add(Operation.INCR);
		cGen.generate(list, System.out);
		String expectedInstructions = "(*p)++;";

		// Find the line that begins code generation.
		while (!br.readLine().contains("// Code generation from BrainF*ck to C begins here"))
			;
		assertTrue(br.readLine().contains(expectedInstructions));
	}

	@Test
	public void decrTest() throws Exception {

		list.add(Operation.DECR);
		cGen.generate(list, System.out);
		String expectedInstructions = "(*p)--;";

		// Find the line that begins code generation.
		while (!br.readLine().contains("// Code generation from BrainF*ck to C begins here"))
			;
		assertTrue(br.readLine().contains(expectedInstructions));
	}

	@Test
	public void jumpTest() throws Exception {

		list.add(Operation.JUMP);
		cGen.generate(list, System.out);
		String expectedInstructions = "while (*p) {";

		// Find the line that begins code generation.
		while (!br.readLine().contains("// Code generation from BrainF*ck to C begins here"))
			;
		assertTrue(br.readLine().contains(expectedInstructions));
	}

	@Test
	public void backTest() throws Exception {

		list.add(Operation.BACK);
		cGen.generate(list, System.out);
		String expectedInstructions = "}";

		// Find the line that begins code generation.
		while (!br.readLine().contains("// Code generation from BrainF*ck to C begins here"))
			;
		assertTrue(br.readLine().contains(expectedInstructions));
	}

	@Test
	public void leftTest() throws Exception {

		list.add(Operation.LEFT);
		cGen.generate(list, System.out);
		String expectedInstructions = "p--;";

		// Find the line that begins code generation.
		while (!br.readLine().contains("// Code generation from BrainF*ck to C begins here"))
			;
		assertTrue(br.readLine().contains(expectedInstructions));
	}

	@Test
	public void rightTest() throws Exception {

		list.add(Operation.RIGHT);
		cGen.generate(list, System.out);
		String expectedInstructions = "p++;";

		// Find the line that begins code generation.
		while (!br.readLine().contains("// Code generation from BrainF*ck to C begins here"))
			;
		assertTrue(br.readLine().contains(expectedInstructions));
	}

	@Test
	public void inTest() throws Exception {

		list.add(Operation.IN);
		cGen.generate(list, System.out);
		String expectedInstructions = "(*p) = getchar();";

		// Find the line that begins code generation.
		while (!br.readLine().contains("// Code generation from BrainF*ck to C begins here"))
			;
		assertTrue(br.readLine().contains(expectedInstructions));
	}

	@Test
	public void outTest() throws Exception {

		list.add(Operation.OUT);
		cGen.generate(list, System.out);
		String expectedInstructions = "putchar(*p);";

		// Find the line that begins code generation.
		while (!br.readLine().contains("// Code generation from BrainF*ck to C begins here"))
			;
		assertTrue(br.readLine().contains(expectedInstructions));
	}

	/**
	 * This tests whether the code generator is capable of translating all the
	 * instructions in the correct order.
	 */
	@Test
	public void allInstructionsTest() throws Exception {

		list.add(Operation.INCR);
		list.add(Operation.DECR);
		list.add(Operation.LEFT);
		list.add(Operation.RIGHT);
		list.add(Operation.IN);
		list.add(Operation.OUT);
		list.add(Operation.JUMP);
		list.add(Operation.BACK);
		cGen.generate(list, System.out);
		String expectedInstructions[] = { "(*p)++;", "(*p)--;", "p--;", "p++;", "(*p) = getchar();", "putchar(*p);", "while (*p) {", "}" };

		// Find the line that begins code generation.
		while (!br.readLine().contains("// Code generation from BrainF*ck to C begins here"))
			;
		// Not using a for Loop, so we know which line is not being represented
		// in case of an error.
		assertTrue(br.readLine().contains(expectedInstructions[0]));
		assertTrue(br.readLine().contains(expectedInstructions[1]));
		assertTrue(br.readLine().contains(expectedInstructions[2]));
		assertTrue(br.readLine().contains(expectedInstructions[3]));
		assertTrue(br.readLine().contains(expectedInstructions[4]));
		assertTrue(br.readLine().contains(expectedInstructions[5]));
		assertTrue(br.readLine().contains(expectedInstructions[6]));
		assertTrue(br.readLine().contains(expectedInstructions[7]));
	}
}