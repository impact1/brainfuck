package impact.brainfuck.check;

import impact.brainfuck.check.LoopChecker;
import impact.brainfuck.exception.LoopException;
import impact.brainfuck.operation.Operation;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * @author Guillaume Andre, Elliot Kauffmann
 * @version 2.1
 *          Test for LoopChecker.
 */
public class LoopCheckerTest {

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Test //(expected = LoopException.class)
    public void testCorrectLoops() throws LoopException {
        Map<Integer, Integer> reference = new HashMap<>();
        reference.put(0, 3); reference.put(1, 2); reference.put(2, 1); reference.put(3, 0);
        List<Operation> instructions = Arrays.asList(Operation.JUMP, Operation.JUMP, Operation.BACK, Operation.BACK);
        Map<Integer, Integer> testJT = new LoopChecker().check(instructions);
        assertEquals(reference, testJT);
    }

    @Test //(expected = LoopException.class)
    public void testOnlyBackStatement() throws LoopException {
        List<Operation> instructions = Collections.singletonList(Operation.BACK);
        LoopChecker lp = new LoopChecker();
        exit.expectSystemExitWithStatus(4);
        lp.check(instructions);
    }

    @Test //(expected = LoopException.class)
    public void testOnlyJumpStatement() throws LoopException {
        List<Operation> instructions = Collections.singletonList(Operation.JUMP);
        LoopChecker lp = new LoopChecker();
        exit.expectSystemExitWithStatus(4);
        lp.check(instructions);
    }

    @Test //(expected = LoopException.class)
    public void testTooManyBackStatements() throws LoopException {
        List<Operation> instructions = Arrays.asList(Operation.JUMP, Operation.JUMP, Operation.BACK, Operation.BACK, Operation.BACK);
        LoopChecker lp = new LoopChecker();
        exit.expectSystemExitWithStatus(4);
        lp.check(instructions);
    }

    @Test //(expected = LoopException.class)
    public void testTooManyJumpStatements() throws LoopException {
        List<Operation> instructions = Arrays.asList(Operation.JUMP, Operation.JUMP, Operation.JUMP, Operation.BACK, Operation.BACK);
        LoopChecker lp = new LoopChecker();
        exit.expectSystemExitWithStatus(4);
        lp.check(instructions);
    }

}
