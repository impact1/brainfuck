package impact.brainfuck.interpreter;

import impact.brainfuck.check.LoopChecker;
import impact.brainfuck.exception.*;
import impact.brainfuck.macro.FunctionCall;
import impact.brainfuck.main.Metrics;
import impact.brainfuck.main.TraceLog;
import impact.brainfuck.memory.Memory;
import impact.brainfuck.operation.*;

import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * @author Elliot Kauffmann, Guillaume Andre, Jeremy Lara
 * @version 1.1
 *          <p>
 *          Interpreter reads files from the reader and uses operations to modify the
 *          memory.
 */
public class Interpreter {

    // Memory file given by main
    private Memory memory;
    // For Loop Iterator : indicates the index of the current instruction
    private int currentInstructionIndex;
    // List of instructions provided by the reader
    private List<Operation> instructions;
    // List of functions provided by the reader
    private Map<Integer, List<FunctionCall>> functions;
    // -i input file
    private BufferedReader br = null;
    // output stream (file or System.out)
    private BufferedReader output;
    // Scanner for inputs
    private Scanner sc = null;
    // --trace option
    private TraceLog traceLog;
    private boolean log = false;
    // Metrics
    private Metrics metrics;


    /**
     * Executes a brainfuck instructions and functions.
     *
     * @param memory       - the memory used for this interpretation
     * @param instructions - the list of the instructions of the program
     * @param input        - the file containing the characters for IN (if -i was used)
     * @param output       - the output file if specified (-o)
     * @param log          - the log file
     * @param jumpTable    - the map associating each JUMP to its BACK
     * @param functions    - the functions to execute for this interpretation
     * @param metrics      - metrics class to save interpretation statistics
     * @throws OutputException - if unable to write to output
     * @throws InputException - if unable to retrieve input
     */
    public Interpreter(Memory memory, List<Operation> instructions, File input, PrintStream output, File log,
                       Map<Integer, Integer> jumpTable, Map<Integer, List<FunctionCall>> functions, Metrics metrics) throws OutputException, InputException {
        this.memory = memory;
        this.metrics = metrics;
        this.currentInstructionIndex = 0;
        this.instructions = instructions;
        metrics.PROG_SIZE = instructions.size();
        this.functions = functions;
        OperationJUMP.setJumpTable(jumpTable);
        OperationBACK.setJumpTable(jumpTable);
        try {
            if (input != null) {
                br = new BufferedReader(new FileReader(input));
                OperationIN.setBr(br);
            } else {
                sc = new Scanner(System.in);
                OperationIN.setSc(sc);
            }
        } catch (IOException e) {
            throw new InputException();
        }

        OperationOUT.setOutStream(output);

        try {
            if (log != null) {
                this.traceLog = new TraceLog(new BufferedWriter(new FileWriter(log)), this, memory);
                this.log = true;
            }
        } catch (IOException e) {
            throw new OutputException();
        }
    }

    public int getCurrentInstructionIndex() {
        return currentInstructionIndex;
    }

    /**
     * Runs the series of instructions provided by the reader, stored in
     * instructions. writes them in memory and uses currentInstructionIndex to
     * know what operation it's at. Operations throw exceptions when used
     * incorrectly. When the program is done running, closes the BufferedReader,
     * effectively saving the output to the -o file.
     */
    public void run()
            throws SyntaxErrorException, PointerException, LoopException, OutputException, CellOverflowException, InputException, AsciiException, OutOfMemory {
        while (currentInstructionIndex < instructions.size()) {
            metrics.measureMetrics(instructions.get(currentInstructionIndex));
            if (functions.containsKey(currentInstructionIndex)) {
                metrics.PROG_SIZE++;
                executeFunctions(functions.get(currentInstructionIndex));
            }
            currentInstructionIndex = instructions.get(currentInstructionIndex).getOperation().doOperation(memory, currentInstructionIndex);
            if (log)
                traceLog.traceLog();
        }

        if (br != null) {
            try {
                br.close();
            } catch (IOException e) {
                throw new InputException();
            }
        }
        if (sc != null) {
            sc.close();
        }

        try {
            output.close();
        } catch (IOException e) {
            throw new OutputException();
        } catch (NullPointerException e) {
            /*
             * For JUnit tests that use System.out as a PrintStream, throws a
			 * NullPointerException when you try to close it.
			 */
        }

        if (functions.containsKey(currentInstructionIndex)) {
            metrics.PROG_SIZE++;
            executeFunctions(functions.get(currentInstructionIndex));
        }

        if (log)
            traceLog.closeTraceLog();
    }

    /**
     * Executes a list of functions.
     *
     * @param functionCalls - the list of the functions calls to be executed
     * @throws SyntaxErrorException  - if the parameters used were incorrect
     * @throws LoopException         - if the JUMP/BACK contained in the function instructions are not properly paired
     * @throws OutputException       - if unable to use OUT
     * @throws InputException        - if unable to use IN
     * @throws CellOverflowException - if a memory cell overflows after INCR
     * @throws PointerException      - if the memory pointer goes out of bounds
     * @throws AsciiException        - if one of the character queried with IN was non-ASCII
     * @throws OutOfMemory           - if the function has not enough memory to initialize
     */
    private void executeFunctions(List<FunctionCall> functionCalls) throws LoopException, OutputException, InputException, CellOverflowException, PointerException, AsciiException, OutOfMemory, SyntaxErrorException {
        for (FunctionCall functionCall : functionCalls) {
            short previousMemoryIndex = memory.getCurrentCellIndex();
            byte oldvalue = memory.getCurrentMemoryCell();
            int index = 0;
            int previousMinLimit = CellOverflowException.minLimit;
            CellOverflowException.minLimit = memory.findLastUnusedCell();
            memory.setMemoryPointer((short) CellOverflowException.minLimit);
            memory.setCurrentMemoryCell(oldvalue);
            Map<Integer, List<FunctionCall>> nestedFunctions = functionCall.getNestedFunctions();
            List<Operation> operationList = functionCall.loadList();
            BufferedReader previousBufferedReader = null;
            if (functionCall.getInput() != null) {
                previousBufferedReader = OperationIN.getBr();
                OperationIN.setBr(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(functionCall.getInput().getBytes()))));
            }
            Map<Integer, Integer> previousJumpTable = OperationJUMP.getJumpTable();
            OperationJUMP.setJumpTable(new LoopChecker().check(operationList));
            OperationBACK.setJumpTable(new LoopChecker().check(operationList));
            while (index < operationList.size()) {
                if (nestedFunctions.containsKey(index)) {
                    executeFunctions(nestedFunctions.get(index));
                }
                metrics.measureMetrics(operationList.get(index));
                index = operationList.get(index).getOperation().doOperation(memory, index);
                if (log)
                    traceLog.traceLog();
            }
            if (nestedFunctions.containsKey(index)) {
                executeFunctions(nestedFunctions.get(index));
            }
            if (functionCall.hasOutput()) {
                memory.setMemoryCellWithCurrentCell(previousMemoryIndex);
            }
            memory.cleanFunctionMemory(CellOverflowException.minLimit);
            memory.setMemoryPointer(previousMemoryIndex);
            OperationJUMP.setJumpTable(previousJumpTable);
            OperationBACK.setJumpTable(previousJumpTable);
            CellOverflowException.minLimit = previousMinLimit;
            if (functionCall.getInput() != null) {
                OperationIN.setBr(previousBufferedReader);
            }
        }


    }

}
