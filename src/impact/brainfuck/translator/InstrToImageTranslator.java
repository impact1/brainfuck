package impact.brainfuck.translator;

import impact.brainfuck.exception.OutputException;
import impact.brainfuck.exception.SyntaxErrorException;
import impact.brainfuck.operation.Operation;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Elliot Kauffmann
 * @version 1.0
 *
 * this class translates an list of instructions to a bitmap image
 */
public class InstrToImageTranslator implements Translator {

    private Map<Operation, Integer> instructionsToColorCode = new HashMap<>();

    /**
     * Constructor of InstrToImageTranslator
     * Initialize the color code for each operation
     */
    public InstrToImageTranslator() {
        this.instructionsToColorCode.put(Operation.INCR, 0xFFFFFF);
        this.instructionsToColorCode.put(Operation.DECR, 0x4B0082);
        this.instructionsToColorCode.put(Operation.LEFT, 0x9400D3);
        this.instructionsToColorCode.put(Operation.RIGHT, 0x0000FF);
        this.instructionsToColorCode.put(Operation.OUT, 0x00FF00);
        this.instructionsToColorCode.put(Operation.IN, 0xFFFF00);
        this.instructionsToColorCode.put(Operation.JUMP, 0xFF7F00);
        this.instructionsToColorCode.put(Operation.BACK, 0xFF0000);
    }

    /**
     * this method set every pixel from a specified region of an image
     * with a specific color code
     * @param startX the x coordinate to define the region
     * @param startY the y coordinate to define the region
     * @param colorCode the color code to set every pixel of the defined region
     * @param image the image to change
     */
    private static void setRegionRGB(int startX, int startY, int colorCode, BufferedImage image) {
        int[] array = new int[9];
        int i = 0;
        while (i < 9) {
            array[i] = colorCode;
            ++i;
        }
        image.setRGB(startX, startY, 3, 3, array, 0, 0);
    }

    /**
     * this method translates each operation from an instruction list
     * to a bitmap image and stores this image into a specified output file
     * @param params the instruction list to translate
     * @param outputFile the output image after translation
     * @throws SyntaxErrorException if any syntax error is detected in params
     * @throws OutputException if any
     */
    public void translate(List<Operation> params, PrintStream outputFile) throws SyntaxErrorException, OutputException {
        int imageSize = 3 * (int)Math.ceil(Math.sqrt(params.size()));
        BufferedImage image = new BufferedImage(imageSize, imageSize, 5);
        int x = 0;
        int y = 0;
        for (Operation instruction : params) {
            InstrToImageTranslator.setRegionRGB(x, y, this.instructionsToColorCode.get(instruction), image);
            if ((x += 3) < imageSize) continue;
            x = 0;
            y += 3;
        }
        try {
            ImageIO.write(image, "bmp", outputFile);
        }
        catch (IOException e) {
            throw new OutputException();
        }
    }
}

