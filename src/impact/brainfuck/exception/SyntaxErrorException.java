package impact.brainfuck.exception;

import impact.brainfuck.reader.TextReader;

/**
 * Thrown when a syntax error is detected in the brainfuck program.
 * Includes badly defined macros, and unknown characters.
 *
 * @author Guillaume Andre
 * @version 1.0
 */
public class SyntaxErrorException extends Exception {

    private static final long serialVersionUID = -620235821177044910L;

    // deprecated
    public SyntaxErrorException(int currentIndex) {
        super("Syntax error in brainfuck program at instruction " + currentIndex);
        System.out.println(this.getMessage());
        System.exit(5);
    }

    /**
     * @param c - character that was used
     */
    public SyntaxErrorException(char c) {
        super("Syntax error in brainfuck program at line " + TextReader.lineCount +
                ":\nCharacter \"" + c + "\" is not a valid brainf*ck instruction (ASCII: " + (int) c + ").");
        System.out.println(this.getMessage());
        System.exit(5);
    }

    /**
     * @param description - description of the error : macro based, etc.
     */
    public SyntaxErrorException(String description) {
        super("Syntax error in brainfuck program at line " + TextReader.lineCount + ":\n" + description);
        System.out.println(this.getMessage());
        System.exit(5);
    }

}
