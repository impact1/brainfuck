package impact.brainfuck.macro;

import impact.brainfuck.component.BaseComponent;
import impact.brainfuck.component.Component;
import impact.brainfuck.component.ElementComponent;
import impact.brainfuck.exception.SyntaxErrorException;
import impact.brainfuck.operation.Operation;
import impact.brainfuck.reader.TextReader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Guillaume André
 *         <p>
 *         Represents a macro. Stores its component list.
 */
public class Macro {

    protected String regex;
    protected String name;
    protected List<Component> components;
    protected DefinedElements definedElements;
    protected Map<Integer, List<FunctionCall>> functions;
    protected int paramCount;
    protected int inputCount;

    /**
     * Constructs the parameterized macro from the line where it was defined.
     * Builds the list of corresponding components of this macro.
     * Sets up its name.
     *
     * @param df        - the list of defined macros
     * @param functions - the map of all functions call
     * @param line      - the line containing the macro definition
     * @throws SyntaxErrorException - if the line contains incorrect syntax
     */
    public Macro(DefinedElements df, String line, Map<Integer, List<FunctionCall>> functions) throws SyntaxErrorException {
        this.definedElements = df;
        this.functions = functions;
        this.components = new ArrayList<>();
        this.paramCount = 0;
        this.inputCount = 0;
        line = setRegexAndRemoveExtras(line);
        addLine(line);
    }

    /**
     * Checks if any character in the given String is not whitespace.
     * If that's the case, throws a SyntaxErrorException.
     *
     * @param line - the String to analyze
     * @throws SyntaxErrorException - if a non-whitespace character was found
     */
    public static void checkForExtraCharacters(String line) throws SyntaxErrorException {
        line = line.replaceAll("\\s", "");
        if (!line.equals("")) {
            throw new SyntaxErrorException("Extra characters in element definition: \"" + line + "\".");
        }
    }

    /**
     * Sets up the regex to extract the name of the macro from the given line.
     * Extracts the name and returns the same line, without the name of the macro.
     *
     * @param line - the line of the macro definition
     * @return the same line, without the name of the macro
     * @throws SyntaxErrorException - if the name syntax is incorrect
     */
    public String setRegexAndRemoveExtras(String line) throws SyntaxErrorException {
        regex = "^[a-z0-9_]+\\?";
        line = extractName(line);
        return line;
    }

    public String getName() {
        return name;
    }

    public int getParamCount() {
        return paramCount;
    }

    public int getInputCount() {
        return inputCount;
    }

    /**
     * Macros do not allow for custom input.
     *
     * @return false
     */
    public boolean isDefaultInput() {
        return false;
    }

    /**
     * Extracts the parameterized macro name from the given String.
     * Sets it as this macro's name.
     * Removes any existing macros with this name from the list of defined macros.
     * Returns the same String, but without the macro name.
     *
     * @param line - the line containing the macro name
     * @return the same line, without the name
     * @throws SyntaxErrorException - if no parameterized macro name could be found in this String
     */
    public String extractName(String line) throws SyntaxErrorException {
        line = line.trim();
        Matcher matcher;
        Pattern findMacroName = Pattern.compile(regex);
        matcher = findMacroName.matcher(line);
        if (matcher.find()) {
            name = matcher.group().substring(0, matcher.group().length() - 1);
            if (name.endsWith("!")) {
                name = name.substring(0, name.length() - 1);
            }
            Macro exists = definedElements.getMacro(name);
            if (exists != null) {
                throw new SyntaxErrorException("Element \"" + name + "\" is already defined.");
            }
            line = matcher.replaceFirst("");
        } else {
            throw new SyntaxErrorException("Incorrect element definition. Macros and functions may not use any upper case letters (only " +
                    "lower case, numbers and \"_\").");
        }
        return line;
    }

    /**
     * Scans a line of brainfuck macro definition.
     * Adds its components to this macro.
     *
     * @param line - the line to be analyzed
     * @throws SyntaxErrorException - if the line contains incorrect syntax
     */
    public void addLine(String line) throws SyntaxErrorException {
        line = line.trim();
        if (line.contains("/")) {
            if (!line.endsWith("/")) {
                throw new SyntaxErrorException("Instructions are not allowed after the end of an element definition (\"/\") on the same line.");
            } else {
                line = line.substring(0, line.length() - 1);
            }
        }
        if (seekLongSyntax(line)) {
            return;
        }
        Pattern findComponents = Pattern.compile("[a-z0-9_]+([%*0-9()&]+)*(\"[^\"]+\")*|[+-.,><\\[\\]]+");
        Matcher matcher = findComponents.matcher(line);
        while (matcher.find()) {
            char first = matcher.group().charAt(0);
            if (Character.isLowerCase(first) || Character.isDigit(first) || first == '_') {
                addComponent(matcher.group());
            } else {
                components.add(new BaseComponent(TextReader.parseSymbols(matcher.group())));
            }
            line = matcher.replaceFirst("");
            matcher = findComponents.matcher(line);
        }
        checkForExtraCharacters(line);
    }

    /**
     * Searches for long instructions brainfuck syntax in the given String.
     * If one is found, adds it to this macro/function component list.
     *
     * @param line - the String to analyze
     * @return true if long syntax was found and added, false otherwise
     */
    public boolean seekLongSyntax(String line) {
        Pattern longSyntax = Pattern.compile("^[A-Z]+$");
        Matcher longMatcher = longSyntax.matcher(line);
        if (longMatcher.find()) {
            for (Operation o : Operation.values()) {
                if (line.equals(o.name())) {
                    components.add(new BaseComponent(Collections.singletonList(o)));
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Adds a component to this macro.
     * For example, d%? a* b% is made of components a* and b%.
     *
     * @param expression - the expression String to be added
     * @throws SyntaxErrorException - if the expression contains a syntax error
     */
    public void addComponent(String expression) throws SyntaxErrorException {
        ElementComponent mc = new ElementComponent(expression, definedElements);
        components.add(mc);
        inputCount += mc.getInputsRequired();
        paramCount += mc.getParamRequired();
    }

    /**
     * Transforms this macro into its corresponding instructions list, and adds the function calls
     * of the function components if they exist.
     *
     * @param parameters      - the parameters of the macro/function, acquired in the brainfuck code
     * @param inputs          - this function custom input
     * @param instructionSize - the number of instructions parsed before this macro/function
     * @param nestedFunctions - the map the function calls should be added to
     * @param atExecTime      - whether this call was done at execution time or not - allows recursive functions
     * @return the corresponding instructions list if this was a macro
     * @throws SyntaxErrorException - if the number of parameters is incorrect
     */
    public List<Operation> toList(List<Integer> parameters, List<String> inputs, int instructionSize,
                                  Map<Integer, List<FunctionCall>> nestedFunctions, boolean atExecTime) throws SyntaxErrorException {
        List<Operation> result = new ArrayList<>();
        for (Component component : components) {
            result.addAll(component.componentToList(parameters, instructionSize + result.size(), functions, inputs));
            parameters = component.getParametersAfterToList();
            inputs = component.getInputsAfterToList();
        }
        return result;
    }

    @Override
    public String toString() {
        return name;
    }

}
