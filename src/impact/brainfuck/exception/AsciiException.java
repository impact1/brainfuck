package impact.brainfuck.exception;

/**
 * Thrown when input file contains non-ASCII characters.
 * 
 * @author Guillaume Andre
 * @version 1.0
 */
public class AsciiException extends Exception {

	private static final long serialVersionUID = -6791119356062940031L;

	public AsciiException() {
		super("Input file contains non ascii character.");
		System.out.println(this.getMessage());
		System.exit(10);
	}

}
