package impact.brainfuck.main;

import impact.brainfuck.operation.Operation;

import static java.lang.Math.pow;

/**
 * @author Florian Bourniquel, Jérémy Lara
 * @version 1.1
 *
 * this class manages metrics' measurement and their display
 */
public class Metrics {

    public static boolean metricsOn = false;

    /**
     * the number of instructions in the program
     */
    public int PROG_SIZE;

    /**
     * the execution time of the program, in milliseconds
     */
    public static long EXEC_TIME;

    /**
     * the number of times the execution pointer was moved to execute this program
     */
    private int EXEC_MOVE;

    /**
     * the number of time the data pointer was moved to execute this program
     */
    private int DATA_MOVE;

    /**
     * the number of time the memory was accessed to change its contents
     * (i.e.,INCR, DECR & IN) while executing this program
     */
    private int DATA_WRITE;

    /**
     * the number of times the memory was accessed to read its contents
     * (i.e., JUMP,BACK, OUT)
     */
    private int DATA_READ;

    /**
     * This method provides metrics measurement.
     *
     * @param operation the operation which determines which metrics has to be
     * incremented.
     */
    public void measureMetrics(Operation operation) {
        EXEC_MOVE++;
        switch (operation) {
            case LEFT:
            case RIGHT:
                DATA_MOVE++;
                break;
            case IN:
            case INCR:
            case DECR:
                DATA_WRITE++;
                break;
            case OUT:
            case JUMP:
            case BACK:
                DATA_READ++;
                break;
            default:
                break;
        }
    }

    /**
     * this method displays the metrics after their measurement
     * @return string which displays metrics
     */
    public String displayMetrics() {
        if (metricsOn) {
            StringBuilder metrics = new StringBuilder();
            metrics.append("\nThis program contains ").append(PROG_SIZE).append(" instruction(s) and was executed in ")
                    .append((int) (EXEC_TIME * pow(10, -6))).append(" ms.\n").append("The execution pointer was moved ").append(EXEC_MOVE)
                    .append(" time(s), and the data pointer ").append(DATA_MOVE).append(" time(s).\n").append("The memory was changed ")
                    .append(DATA_WRITE).append(" time(s) and was read ").append(DATA_READ).append(" time(s).\n");
            metricsOn = false;
            return String.valueOf(metrics);
        }
        return "";
    }
}
