package impact.brainfuck.reader;

import impact.brainfuck.exception.NotFoundException;
import impact.brainfuck.macro.FunctionCall;
import impact.brainfuck.operation.Operation;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

/**
 * @author Jeremy Lara
 * @version 1.1
 *
 * This Class provides reading bitmap image
 *
 */
public class ImageReader implements Reader{

    File file;
    private static Map<String, String> colorToShortSyntax = new HashMap<>();

    //this static block prevents the program from adding the content of the colorToShortSyntax
    // every time a new ImageReader object is created. It also provide the readImage() method from having
    // eight 'if' condition blocks and thus saving many operations.
    static {
        colorToShortSyntax.put("FFFFFF", "INCR");
        colorToShortSyntax.put("4B0082", "DECR");
        colorToShortSyntax.put("9400D3", "LEFT");
        colorToShortSyntax.put("0000FF", "RIGHT");
        colorToShortSyntax.put("00FF00", "OUT");
        colorToShortSyntax.put("FFFF00", "IN");
        colorToShortSyntax.put("FF7F00", "JUMP");
        colorToShortSyntax.put("FF0000", "BACK");
    }

    /**
     * Constructor of ImageReader class.
     *
     * @param file the input file; has to be a bitmap image.
     */
    public ImageReader(File file) { this.file = file; }

    /**
     * This method reads a bitmap image input and stock the instructions read from an ArrayList<Operation>.
     *
     * @return A string whose the contents is short syntax brainfuck's keywords.
     * @throws NotFoundException
     *
     * This method handles NotFoundException
     */
    public List<Operation> read() throws NotFoundException {

        List<Operation> instructions = new ArrayList<>();
        BufferedImage image;
        int width;
        int height;

        try {

            image = ImageIO.read(file);
            width = image.getWidth();
            height = image.getHeight();

            for (int i = 0; i < height; i += 3) {
                for (int j = 0; j < width; j += 3) {
                    Color c = new Color(image.getRGB(j, i));
                    //the line's code below converts int values to a hex value.
                    String hex = String.format("%02X%02X%02X", c.getRed(), c.getGreen(), c.getBlue());
                    if (colorToShortSyntax.containsKey(hex)) {
                        instructions.add(Operation.valueOf(colorToShortSyntax.get(hex)));
                    }
                }
            }
        } catch (IOException e) {
            throw new NotFoundException(file.getPath());
        } catch (NullPointerException e) {
            return instructions;
        }
        return instructions;
    }

    @Override
    public Map<Integer, List<FunctionCall>> getFunctions() {
        return new HashMap<>();
    }
}
