package impact.brainfuck.exception;

/**
 * Thrown when we try to execute a function without memory space
 * file.
 *
 * @author Florian Bourniquel
 * @version 1.0
 */
public class OutOfMemory extends Exception{

	private static final long serialVersionUID = -6857134567296699320L;

	public OutOfMemory() {
        super("Out of memory.");
        System.out.println(this.getMessage());
        System.exit(9);
    }
}
