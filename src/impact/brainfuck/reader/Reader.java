package impact.brainfuck.reader;

import impact.brainfuck.exception.BadArgumentsException;
import impact.brainfuck.exception.NotFoundException;
import impact.brainfuck.exception.ReadException;
import impact.brainfuck.exception.SyntaxErrorException;
import impact.brainfuck.macro.FunctionCall;
import impact.brainfuck.operation.Operation;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

/**
 * Interface Reader for any type of reader
 *
 * @author Jeremy Lara
 * @version 1.1
 */
public interface Reader  {

    List<Operation> read() throws FileNotFoundException, NotFoundException, SyntaxErrorException, ReadException, BadArgumentsException;

    Map<Integer, List<FunctionCall>> getFunctions();

}
